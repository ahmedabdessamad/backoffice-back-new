## Development
### Get server up and running
in order to start seeing the components run Storybook:
```sh
npm run dev
```

### Create new entity
`npm run create-entity [entityName]`: This command scaffolds entity resolver and schema files.
You can use it as follow:
```
npm run create-entity testEntity
```

### Server folders structure explained
Every entity will have the following files
 ```
 /src/resolvers
   └── entityName.resolvers.js
 /src/typeDefs
   └── entityName.graphql
 ```
 - The `resolvers` folder contains `.resolvers.js` files that represent the implementation of the entity resolvers.
 - The `typeDefs` folder contains the `.graphql` schema definition of the component.
 - The schemas and resolvers will be automatically merged, so no need to import them manually.
 
 ### TODOS
 - Add prettier
 - Add eslint