const { hasGroup } = require("../src/helpers/auth");

describe("hasGroup function", () => {
  it("should return true if user has the right group", () => {
    const user = { permission: [ { groupe: { name: "Sales" } } ]};
    const groupe = "Sales";

    expect(hasGroup(user, groupe)).toBe(true);
  });

  it("should return false if user doesn't have the group", () => {
    const user = { permission: [ { groupe: { name: "Sales" } } ]};
    const groupe = "XXX";

    expect(hasGroup(user, groupe)).toBe(false);
  });

  it("should return false if user has no permissions", () => {
    const user = { permission: []};
    const groupe = "XXX";

    expect(hasGroup(user, groupe)).toBe(false);
  });

  it("should throw error if permission is undefined", () => {
    const user = {};
    const groupe = "XXX";

    expect(() => hasGroup(user, groupe)).toThrow();
  });
});