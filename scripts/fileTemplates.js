"use strict";

exports.resolverTemplate = ({ entityName }) => {
  const entityNameCapitalInitial =
    entityName.charAt(0).toUpperCase() + entityName.slice(1);
  const moduleName = entityName + "Resolvers";
  return `
const ${moduleName} = {
  Query: {

  },
  Mutation: {

  },
  ${entityNameCapitalInitial}: {

  }
};

module.exports = ${moduleName};

`.trim();
};

exports.modelTemplate = ({ entityName }) => {
  const entityNameCapitalInitial =
    entityName.charAt(0).toUpperCase() + entityName.slice(1);

  return `
var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var ${entityNameCapitalInitial}Schema = new Schema(
  {

  },
  { collection: "${entityName}" }
);

module.exports = mongoose.model("${entityName}", ${entityNameCapitalInitial}Schema);

`.trim();
};

exports.schemaTemplate = ({ entityName }) => {
  const entityNameCapitalInitial =
    entityName.charAt(0).toUpperCase() + entityName.slice(1);

  return `
type ${entityNameCapitalInitial} {

}

type Query {

}

type Mutation {

}

`.trim();
};
