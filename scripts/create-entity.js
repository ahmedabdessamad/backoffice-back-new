#!/usr/bin/env node
/**
 * This script scaffolds graphql entities.
 *
 * For the following command:
 *
 * `npm run create-entity entityName`
 *
 * The following file tree will be generated:
 *
 * /src/resolvers
 * |  └── entityName.resolvers.js
 * /src/typeDefs
 * |  └── entityName.graphql
 *
 */

const path = require("path");
const fs = require("fs-extra");
const task = require("./task");

const templates = require("./fileTemplates");

const resolversDir = "src/resolvers";
const schemasDir = "src/typeDefs";

module.exports = task("create-entity", async () => {
  const entityName = process.argv[2];

  console.info("creating entity...");
  await createEntityFiles({ entityName });
  console.info("entity created!");
});

async function createEntityFiles({ entityName }) {
  if (!entityName) {
    throw new Error(
      "Missing entity name argument, use: `npm run create-entity [entityName]`"
    );
  }

  // Create resolvers file
  await fs.writeFile(
    path.join(resolversDir, `${entityName}.resolvers.js`),
    templates.resolverTemplate({ entityName })
  );

  // Create schema file
  await fs.writeFile(
    path.join(schemasDir, `${entityName}.graphql`),
    templates.schemaTemplate({ entityName })
  );
}
