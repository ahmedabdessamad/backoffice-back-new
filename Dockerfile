FROM node:12.21-buster

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn

COPY . .

EXPOSE 7899

CMD ["yarn","run","dev"]

