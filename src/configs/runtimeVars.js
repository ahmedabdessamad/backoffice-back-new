import { config } from "dotenv";
import slashlessUrl from "./slashlessUrl";
import { requiredEnvCheck, interestingVarsCheck } from "./helpers";

config();

// check env vars
requiredEnvCheck();
interestingVarsCheck();

const runtimeVars = {
  MS_NAME: "GATEWAY_BACKOFFICE",
  NODE_ENV: process.env.NODE_ENV || "development",
  PORT: process.env.PORT || 7898,
  SECRET_WORD: process.env.SECRET_WORD,
  DB: process.env.DB,
  CLIENT_HOST: slashlessUrl(process.env.CLIENT_HOST),
  HOST:
    slashlessUrl(process.env.HOST) ||
    `http://localhost:${process.env.PORT || 7898}`, // the port must be identical to the port above
  SERVER_REGISTRY_LINK: process.env.SERVER_REGISTRY_LINK, // service registry uri
  SERVER_REGISTRY_SECRET: process.env.SERVER_REGISTRY_SECRET, // public secret,
  FORMS_HOST: slashlessUrl(process.env.FORMS_HOST), // url of forms host sent to consultants,
  XERO: {
    CLIENT_ID: process.env.XERO_CLIENT_ID || "",
    CLIENT_SECRET: process.env.XERO_CLIENT_SECRET || "",
    SCOPES: process.env.XERO_SCOPES || "",
    REDIRECT_URL: process.env.XERO_REDIRECT_URL || ""
  },
  BH: {
    CLIENT_ID: process.env.BH_CLIENT_ID || "",
    CLIENT_SECRET: process.env.BH_CLIENT_SECRET || "",
    USERNAME: process.env.BH_USERNAME || "",
    PASSWORD: process.env.BH_PASSWORD || "",
    TTL: process.env.BH_TTL || 2880
  },
  NUMBER_OF_RETRY: 100, // Number of retry to get data from bh if error is too many requests
  TIME_OUT: 1000 // wait time to re-execute function call from bullhorn if error is too many requests
};

export default runtimeVars;
