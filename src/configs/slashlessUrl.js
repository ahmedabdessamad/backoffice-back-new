const FormatError = new Error("URL IN BAD FORMAT");

const slashlessUrl = (url) => {
  if (!url || typeof url !== "string") throw FormatError;
  const cleanUrl = url.trim().toLocaleLowerCase();
  if (!cleanUrl) throw FormatError;
  if (cleanUrl.endsWith("/")) return cleanUrl.slice(0, -1);
  return cleanUrl;
};

export default slashlessUrl;
