module.exports = {
  emailSender: process.env.CF_SENDER_EMAIL || "no-reply@club-freelance.com",
  templates: {
    clientOnSendoutValidation: {
      fr: 1269511,
      uk: 1269511
    }
  },
  financeReceiver: process.env.CF_FIN_EMAIL || "fin@mailinator.com"
};
