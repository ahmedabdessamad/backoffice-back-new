const interestingVars = ["FORMS_HOST"];

const interestingVarsCheck = () => {
  interestingVars.forEach((envVar) => {
    if (!process.env[envVar]) {
      console.warn("🚫 interesting env attribute is missing:", envVar);
    }
  });
};

export default interestingVarsCheck;
