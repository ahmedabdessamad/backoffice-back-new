const requiredVars = [
  "DB",
  "SECRET_WORD",
  "CLIENT_HOST",
  "SERVER_REGISTRY_LINK",
  "SERVER_REGISTRY_SECRET"
];

const requiredEnvCheck = () => {
  requiredVars.forEach((envVar) => {
    if (!process.env[envVar]) {
      console.error("🚫 required env attribute is missing:", envVar);
      process.exit(1);
    }
  });
};

export default requiredEnvCheck;
