const express = require("express");
const bodyParser = require("body-parser");
// const util = require('util');
const crypto = require("crypto");
const {
  initiateXero,
  updateRepeatingInvoice,
  getInvoice
} = require("../helpers/xeroHelpers");
const models = require("../models");
const { authMiddleware } = require("./timesheetAuthMiddleware");
const decode = require("../helpers/decode");
const cron = require("../models/cron");

const { invoice, tokenSet, batchPayment } = models;
const router = express.Router();

const drafts = (invoice) => {
  const invoices = {
    invoices: [
      {
        type: invoice.type,
        contact: {
          contactID: invoice.contact.contactID
        },
        // dueDate: invoice.dueDate,
        lineItems: invoice.lineItems,
        status: "SUBMITTED"
      }
    ]
  };
  console.log("Invoice = ", invoices);
  return invoices;
};
const upsertInvoice = async (invoiceId, tenantId) => {
  const invoiceFromXero = await getInvoice(invoiceId, tenantId);
  console.log("invoice from xero =", invoiceFromXero);
  const invoiceToInsert = await invoice.findOneAndUpdate(
    { invoiceID: invoiceId },
    invoiceFromXero,
    { upsert: true, setDefaultsOnInsert: true }
  );
  console.log("invoice added to invoice collection:", invoiceToInsert);
};

router.use("/sas/callback", async function (req, res) {
  const xero = await initiateXero();
  const TokenSet = await xero.apiCallback(req.url);
  const xeroTenants = await xero.updateTenants();

  tokenSet.findOneAndUpdate(
    {},
    { ...TokenSet, tenants: xeroTenants },
    {
      upsert: true
    },
    (err) => {
      if (err) console.error(err);
    }
  );
  const cr = await cron.findOneAndUpdate(
    {},
    { error: false, cronDate: TokenSet.expires_at },
    { new: true }
  );
  console.log(cr);
  res.redirect(`${process.env.CLIENT_HOST}/home/`);
});

router.use("/succ_callback/:batchPaymentID", async (req, res) => {
  const { batchPaymentID } = req.params;
  await batchPayment.findOneAndUpdate(
    { batchPaymentID },
    { validationStatus: "PAID" },
    { new: true }
  );
  res.redirect(`${process.env.CLIENT_HOST}/home/validation?status=success`);
});
router.use("/paymentFailure", (req, res) => {
  res.redirect(`${process.env.CLIENT_HOST}/home/validation?status=failed`);
});
router.post(
  "/webhook",
  bodyParser.raw({ type: "application/json" }),
  async (req, res) => {
    // 1 receive POST HTTP request from Xero
    // 2 validation source query
    // 2.5 fetch invoice from xero api
    // 3 creation d invoice fil base donnee
    //console.log('received Hash: ', req.header('x-xero-signature'));

    const payload = req.body.toString();
    console.log("received payload: ", JSON.parse(payload));
    try {
      const xero_signature = req.headers["x-xero-signature"];
      const hashedPayload = crypto
        .createHmac("sha256", process.env.XERO_WEBHOOK_KEY)
        .update(payload)
        .digest("base64");

      console.log("hash generated: ", hashedPayload);

      if (hashedPayload !== xero_signature) {
        return res.status(401).end();
      } else {
        await Promise.all(
          JSON.parse(payload).events.map((e) =>
            upsertInvoice(e.resourceId, e.tenantId)
          )
        );
        // console.log("invoice added to invoice collection");
        return res.status(200).end();
      }
    } catch (err) {
      console.log("Error = ", err);
      throw Error(err);
    }
  }
);

router.post(
  "/webhook/update-invoice",
  authMiddleware(),
  bodyParser.json(),
  async (req, res) => {
    const data = req.body;

    if (!data || !data.placementID || data.Qty)
      return res
        .status(422)
        .json({ status: "ERROR", message: "INVALID INPUT" });
    try {
      const invoiceDrafts = await invoice
        .find({
          status: "DRAFT",
          "lineItems.itemCode": data.placementID.toString()
        })
        .select({
          __v: 0,
          _id: 0,
          dateString: 0,
          dueDateString: 0,
          updatedDateUTC: 0,
          date: 0
        })
        .lean();

      const modifiedInvoices = invoiceDrafts.map((i) => {
        const matchedLineItem = i.lineItems.find(
          (lineItem) => lineItem.itemCode === `${data.placementID}`
        );

        if (matchedLineItem) {
          matchedLineItem.quantity = data.Qty;
          matchedLineItem.lineAmount = data.Qty * matchedLineItem.unitAmount;
        }

        return i;
      });

      await modifiedInvoices.map((i) => {
        updateRepeatingInvoice(i.invoiceID, i.tenant, drafts(i));
      });

      return res.status(200).json({ status: "OK" });
    } catch (err) {
      console.error(err);
    }
  }
);

/* MS REGISTRY */
router.post("/ms-registry-update", async (req, res) => {
  global.__microservice_registry = decode(req.body.payload);
  console.info("✅ micro services map has been updated successfully");
  return res.status(200).end();
});
module.exports = router;
