const jwt = require("jsonwebtoken");

const authMiddleware = () => {
  return (req, res, next) => {
    const token = req.headers["authorization"];

    // authentication required
    if (!token) return res.status(401).json({ error: "NOT AUTHENTICATED" });

    // extract data from the passed json
    let decodedToken = "";
    try {
      decodedToken = jwt.verify(
        token.slice(7),
        process.env.TIMESHEET_SECRET_WORD
      );
    } catch (err) {
      return res.status(401).json({ error: "NOT AUTHENTICATED/AUTHORISED" });
    }

    // fallback if the auth token is missing the role
    if (!decodedToken.role || decodedToken.role !== "TIMESHEET BOT")
      return res.status(401).json({ error: "NOT AUTHENTICATED/AUTHORISED" });
    next();
  };
};

module.exports = {
  authMiddleware
};
