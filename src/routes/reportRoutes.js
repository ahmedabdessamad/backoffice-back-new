import getFile from "../ms/resources/getJsonReport";
import isAuthenticatedCookie from "../middlewares/isAuthenticatedCookie";
import { checkPermission } from "../helpers/authorization";
import validateInt from "../helpers/validateInt";
import parseCookies from "../helpers/parseCookies";
const express = require("express");

const router = express.Router();

const reportsLink = async (timesheetID) => {
  return await getFile(timesheetID);
};

router.get("/report/:timesheetID", async function (req, res) {
  const cookies = parseCookies(req.headers.cookie);
  if (!cookies)
    return res.redirect(`${process.env.CLIENT_HOST}/home/timesheets/reports`);
  const permission = await isAuthenticatedCookie(cookies);

  if (!permission.loggedIn)
    return res.redirect(`${process.env.CLIENT_HOST}/home/timesheets/reports`);
  try {
    checkPermission({ REPORTS: 7 }, permission.user);
  } catch (e) {
    return res.redirect(`${process.env.CLIENT_HOST}/home/timesheets/reports`);
  }
  try {
    validateInt(req.params.timesheetID);
  } catch (e) {
    return res.redirect(`${process.env.CLIENT_HOST}/home/timesheets/reports`);
  }

  const report = await reportsLink(req.params.timesheetID);
  if (!report || !report.fileBase64)
    return res.redirect(`${process.env.CLIENT_HOST}/home/timesheets/reports`);

  res.writeHead(200, {
    "Content-Type": "application/pdf",
    "Content-Disposition": '"filename="' + `${report.fileName}` + '.pdf"'
  });
  const download = Buffer.from(report.fileBase64.toString(), "base64");
  res.end(download);
});

module.exports = router;
