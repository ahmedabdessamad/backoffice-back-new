const checkPermission = require("./checkPermission");
const checkSpecificPermission = require("./checkSpecificPermission");
const checkPermissionAndGetRole = require("./checkPermissionAndGetRole");
module.exports = {
  checkPermission,
  checkSpecificPermission,
  checkPermissionAndGetRole
};
