const UNAUTHORIZED = new Error("UNAUTHORIZED");
const UNAUTHENTICATED = new Error("UNAUTHENTICATED");
const checkPermission = (requiredPermission, user) => {

  if (!user) throw UNAUTHENTICATED;
  const tabs = user.permissions.map((p) => p.tabs);

  const newTab = tabs
    .flat()
    .find(
      ({ name, permission }) =>
        name in requiredPermission &&
        (permission & requiredPermission[name]) > 0
    );

  if (!newTab) throw UNAUTHORIZED;
};

module.exports = checkPermission;
