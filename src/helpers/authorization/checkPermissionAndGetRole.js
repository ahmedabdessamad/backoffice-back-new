const UNAUTHORIZED = new Error("UNAUTHORIZED");
const UNAUTHENTICATED = new Error("UNAUTHENTICATED");

const checkPermissionAndGetRole = (requiredPermission, user) => {
  if (!user) throw UNAUTHENTICATED;
  const tabs = user.permissions.map((p) => p.tabs);
  console.log("tabs",user)
  const newTabAdmin = tabs
    .flat()
    .find(
      ({ name, permission }) => name in requiredPermission && (permission & 4) === 4
    );
  console.log("newTabAdmin",newTabAdmin);
  if (newTabAdmin) return "Admin";

  const newTabEdit = tabs
    .flat()
    .find(
      ({ name, permission }) => name in requiredPermission && (permission & 2) === 2
    );
  console.log("newTabEdit",newTabEdit);
  if (newTabEdit) return "Editor";



  const newTabRead = tabs
    .flat()
    .find(
      ({ name, permission }) => name in requiredPermission && (permission & 1) === 1
    );
  console.log("newTabRead",newTabRead);
  if (newTabRead) return "Reader";

  throw UNAUTHORIZED;
};

module.exports = checkPermissionAndGetRole;
