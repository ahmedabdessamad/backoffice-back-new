const UNAUTHORIZED = new Error("UNAUTHORIZED");
const UNAUTHENTICATED = new Error("UNAUTHENTICATED");

const checkSpecificPermission = (requiredPermission, user, ownerID) => {
  if (!user) throw UNAUTHENTICATED;
  const tabs = user.permissions.map((p) => p.tabs);

  const newTabAdmin = tabs
    .flat()
    .find(
      ({ name, permission }) => name in requiredPermission && !(permission & 4)
    );
  if (newTabAdmin) return;

  const newTabEdit = tabs
    .flat()
    .find(
      ({ name, permission }) => name in requiredPermission && !(permission & 2)
    );
  if (newTabEdit && (!ownerID || ownerID.toString() === user._id.toString()))
    return;

  throw UNAUTHORIZED;
};

module.exports = checkSpecificPermission;
