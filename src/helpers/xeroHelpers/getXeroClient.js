import { XeroClient } from "xero-node";
import runtimeVars from "../../configs/runtimeVars";

const models = require("../../models");
const isExpired = require("./../isExpired");
const { Cron } = require("../../models");

const { XERO } = runtimeVars;

const tokenSet = models.tokenSet;
const xero = new XeroClient();

async function getXeroClient() {
  const expiredToken = await isExpired();
  const token = await tokenSet.findOne();

  if (token) {
    if (expiredToken) {
      try {
        const newTokenSet = await xero.refreshWithRefreshToken(
          XERO.CLIENT_ID,
          XERO.CLIENT_SECRET,
          token.refresh_token
        );

        // console.log('NEW TOKEN SET =', newTokenSet);
        // console.log('updated token:', newt);
        await xero.setTokenSet(newTokenSet);
        // console.log("tenants1=", tenants);
        await tokenSet.updateOne(
          { _id: token._id },
          {
            id_token: newTokenSet.id_token,
            expires_at: Math.floor(Date.now() / 1000) + 1740,
            access_token: newTokenSet.access_token,
            refresh_token: newTokenSet.refresh_token
          }
        );

        return xero;
      } catch (e) {
        console.error(e);
        // throw new Error(e);
        if (
          e.response.body.Status === 401 ||
          JSON.parse(e.response.body).error === "invalid_grant"
        ) {
          if (Math.floor(Date.now() / 1000) - token.expires_at > 1740) {
            await tokenSet.findOneAndDelete();
          }
          //throw new Error("Invalid_Grant");

          await Cron.findOneAndUpdate(
            {},
            { error: true },
            { new: true, setDefaultsOnInsert: true }
          );
        }
      }
    } else {
      try {
        // console.log(token)
        await xero.setTokenSet(token);
        await xero.updateTenants();
        // console.log("tenants2=", tenants);

        return xero;
      } catch (e) {
        // console.log("Error =", e.response.body);
        if (e.response.body.Status === 401) {
          //throw new Error("Invalid_Grant");

          await Cron.findOneAndUpdate(
            {},
            { error: true },
            { new: true, setDefaultsOnInsert: true }
          );
        }
        //throw new Error(e);
      }
    }
  }
}
module.exports = getXeroClient;
