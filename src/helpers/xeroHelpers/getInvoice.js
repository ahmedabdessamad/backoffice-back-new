const getXeroClient = require("./getXeroClient");

const getInvoicesByID = async (id, tenantId) => {
  const xero = await getXeroClient();
  // const tenants = xero.tenants;
  // const invoices = tenants.filter((t) => t.tenantName === organisation);
  // console.log("invoices=", invoices);
  const apiResponse = await xero.accountingApi.getInvoice(tenantId, id);
  // console.log("api response=", apiResponse.body.invoices[0]);
  apiResponse.body.invoices[0].tenant = tenantId;
  return apiResponse.body.invoices[0];
};

module.exports = getInvoicesByID;
