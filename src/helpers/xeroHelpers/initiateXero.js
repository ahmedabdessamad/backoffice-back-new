import runtimeVars from "../../configs/runtimeVars";
import { XeroClient } from "xero-node";

const { XERO } = runtimeVars;

const initiateXero = async () => {
  const xero = new XeroClient({
    clientId: XERO.CLIENT_ID,
    clientSecret: XERO.CLIENT_SECRET,
    redirectUris: [XERO.REDIRECT_URL],
    scopes: XERO.SCOPES.split(" ")
  });
  await xero.initialize();

  return xero;
};

module.exports = initiateXero;
