import { requestTimesheet } from "../../../ms/timesheet";
import Item from "../../../models/item";
import newItemObject from "./helpers/newItemObject";

const getNewItems = async () => {
  let itemsSAS = [];
  let itemsLTD = [];
  let item = null;
  const { placements } = await requestTimesheet(
    "get",
    "/placements/get/getplacementstocreateitems"
  );
  if (!placements || !placements.length) return;

  for (const pl of placements) {
    try {
      item = await Item.findOne({ placementID: `${pl.id}` });
    } catch (e) {
      continue;
    }
    if (!item) {
      if (!pl.employeeType) continue;
      if (pl.employeeType === "CF limited") {
        try {
          const itemLtd = await newItemObject(pl);
          itemsLTD.push(itemLtd);
        } catch (e) {
          continue;
        }
      } else {
        try {
          const itemSas = await newItemObject(pl);
          itemsSAS.push(itemSas);
        } catch (e) {
          continue;
        }
      }
    }
  }
  return { itemsSAS, itemsLTD };
};

export default getNewItems;
