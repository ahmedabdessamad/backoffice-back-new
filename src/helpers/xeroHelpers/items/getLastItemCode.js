import Item from "../../../models/item";

const getLastItemCode = async () => {
  const items = await Item.find().sort({ placementID: -1 }).limit(1).lean();
  return items[0].placementID;
};

export default getLastItemCode;
