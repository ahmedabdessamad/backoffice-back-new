import { NOT_FOUND } from "../../errors";
import getNewItems from "./getNewItems";
const getXeroClient = require("../getXeroClient");

const models = require("../../../models");
const { tokenSet } = models;

const createItems = async () => {
  const exists = await tokenSet.countDocuments();
  const xero = await getXeroClient();
  let tenantId = null;

  if (!xero) {
    console.warn("no xero client");
    throw NOT_FOUND;
  }
  if (!exists) {
    console.warn("TOKEN SET NOT FOUND");
    throw NOT_FOUND;
  }

  const token = await tokenSet.findOne();

  const { itemsSAS, itemsLTD } = await getNewItems();

  if ((!itemsLTD || !itemsLTD.length) && (!itemsSAS || !itemsSAS.length)) {
    console.info("NO items to set");
    return;
  }
  if (itemsSAS && itemsSAS.length) {
    try {
      tenantId = token.tenants.find(
        (tenant) => tenant.tenantName === "Club Freelance SAS"
      ).tenantId;
      const responseSas = await xero.accountingApi.updateOrCreateItems(
        tenantId,
        {
          items: itemsSAS
        },
        true,
        4
      );
      console.log("responseLTD", responseSas);
    } catch (e) {
      console.error("error SAS", e);
    }
  } else {
    console.info("NO items to set In SAS");
  }
  if (itemsLTD && itemsLTD.length) {
    try {
      tenantId = token.tenants.find(
        (tenant) => tenant.tenantName === "Club Freelance Limited"
      ).tenantId;
      const responseLtd = await xero.accountingApi.updateOrCreateItems(
        tenantId,
        {
          items: itemsLTD
        },
        true,
        4
      );
      console.log("responseLTD", responseLtd);
    } catch (e) {
      console.error("error LTD", e.response.body);
      console.error("error LTD", e.response.body.Elements[0].ValidationErrors);
    }
  } else {
    console.info("NO items to set In LTD");
  }
};

export default createItems;
