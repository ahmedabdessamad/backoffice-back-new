import Item from "../../../models/item";
import getItemPlacementID from "./getItemPlacementID";
import { NOT_FOUND } from "../../errors";

const getXeroClient = require("../getXeroClient");

const models = require("../../../models");
const { tokenSet } = models;

const syncLatestItems = async (cfEntity) => {
  const exists = await tokenSet.countDocuments();
  const xero = await getXeroClient();

  if (!xero) {
    console.warn("no xero client");
    throw NOT_FOUND;
  }
  if (!exists) {
    console.warn("TOKEN SET NOT FOUND");
    throw NOT_FOUND;
  }
  const token = await tokenSet.findOne();
  let tenantId = null;
  if (cfEntity === "SAS") {
    tenantId = token.tenants.find(
      (tenant) => tenant.tenantName == "Club Freelance SAS"
    ).tenantId;
  } else {
    tenantId = token.tenants.find(
      (tenant) => tenant.tenantName == "Club Freelance Limited"
    ).tenantId;
  }
  const { UpdatedDateUTC } = await Item.findOne(
    { cfEntity },
    { Code: 1, UpdatedDateUTC: 1 },
    { sort: "-UpdatedDateUTC", limit: 1 }
  ).lean();
  let lastUpdateTimestamp = null;

  if (UpdatedDateUTC.startsWith("/Date")) {
    lastUpdateTimestamp = parseInt(UpdatedDateUTC.match(/\d+/)[0]);
  } else {
    lastUpdateTimestamp = UpdatedDateUTC;
  }

  const lastUpdateIsoDate = new Date(lastUpdateTimestamp);
  const response = await xero.accountingApi.getItems(
    tenantId,
    lastUpdateIsoDate
  );
  const newItems = response.body.items;

  // We check if we have more than one item, because xero api will always return the item with the filtered update date
  if (newItems.length >= 1) {
    newItems.forEach(async (item) => {
      const placementID = getItemPlacementID(item);
      const updateItem = {
        placementID,
        cfEntity,
        Name: item.name,
        Code: item.code,
        PurchaseDescription: item.purchaseDescription,
        PurchaseDetails: item.purchaseDetails,
        SalesDetails: item.salesDetails,
        UpdatedDateUTC: item.updatedDateUTC
      };
      await Item.findOneAndUpdate({ Code: item.code }, updateItem, {
        upsert: true
      });
    });
  }
};

export default syncLatestItems;
