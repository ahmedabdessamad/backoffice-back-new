const getItemPlacementID = (item) => {
  const { code } = item;

  const extensionRegEx = /^\d+\.\d{1}$/;
  const standardRegEx = /^\d+$/;
  const isStandardOrExtension =
    standardRegEx.test(code) || extensionRegEx.test(code);

  if (isStandardOrExtension) {
    return parseInt(code.split(".")[0]);
  }
};
export default getItemPlacementID;
