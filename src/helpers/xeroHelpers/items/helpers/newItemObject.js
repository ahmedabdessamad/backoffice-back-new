import { NOT_FOUND } from "../../../errors";

const newItemObject = async (placement) => {
  if (!placement.employeeType) throw NOT_FOUND;

  const newItemName = `${placement.id}_${placement.candidate.lastName}_${placement.clientCorporation.name}`;

  if (placement.employeeType === "CF limited") {
    const newItemObj = {
      code: `${placement.id}`,
      name: newItemName,
      description: "generated item",
      purchaseDescription: `${placement.customText12} / TS : ${placement.customText19}`,
      purchaseDetails: {
        unitPrice: Number.isInteger(placement.payRate) ? placement.payRate : 0,
        accountCode: "330",
        taxType: "NONE"
      },
      salesDetails: {
        unitPrice: Number.isInteger(placement.clientBillRate)
          ? placement.clientBillRate
          : 0,
        accountCode: "200",
        taxType: "NONE"
      }
    };
    return newItemObj;
  } else {
    const newItemObj = {
      code: `${placement.id}`,
      name: newItemName,
      description: "generated item",
      purchaseDescription: `${placement.customText12} / TS : ${placement.customText19}`,
      purchaseDetails: {
        unitPrice: Number.isInteger(placement.payRate) ? placement.payRate : 0,
        accountCode: "330",
        taxType: "INPUT"
      },
      salesDetails: {
        unitPrice: Number.isInteger(placement.clientBillRate)
          ? placement.clientBillRate
          : 0,
        accountCode: "200",
        taxType: "INPUT"
      }
    };
    return newItemObj;
  }
  throw NOT_FOUND;
};

export default newItemObject;
