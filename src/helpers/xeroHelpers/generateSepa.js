const RandExp = require("randexp");
const moment = require("moment");
const builder = require("xmlbuilder");

const buildCreditorInformation = (payment) => {
  return {
    PmtId: {
      InstrId: new RandExp(/^([a-zA-Z0-9]{10,35})$/).gen(),
      EndToEndId: new RandExp(/^([a-zA-Z0-9]{10,35})$/).gen()
    },
    Amt: {
      InstdAmt: { "@Ccy": "EUR", "#text": payment.amount }
    },

    Cdtr: {
      Nm: payment.invoice.contact.name
    },
    CdtrAcct: {
      Id: {
        IBAN: payment.invoice.contact.bankAccountDetails
      }
    }
  };
};

const generateSepa = (requestedBatchPayment) => {
  const obj = {
    Document: {
      "@xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
      "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
      "@xmlns": "urn:iso:std:iso:20022:tech:xsd:pain.001.001.03",
      CstmrCdtTrfInitn: {
        GrpHdr: {
          MsgId: new RandExp(/^([a-zA-Z0-9]{10,35})$/).gen(),
          CreDtTm: `${moment().format("YYYY-MM-DDTh:mm:ss")}`,
          NbOfTxs: requestedBatchPayment.payments.length,
          CtrlSum: requestedBatchPayment.totalAmount,
          InitgPty: {
            Id: {
              OrgId: {
                Othr: {
                  Id: "ABC12345001"
                }
              }
            }
          }
        },
        PmtInf: {
          PmtInfId: new RandExp(/^([a-zA-Z0-9]{10,35})$/).gen(),
          PmtMtd: "TRF",
          PmtTpInf: {
            SvcLvl: {
              Cd: "SEPA"
            }
          },
          ReqdExctnDt: `${moment(requestedBatchPayment.dateString).format(
            "YYYY-MM-DD"
          )}`,
          Dbtr: {
            Nm: "CLUB FREELANCE",
            PstlAdr: {
              StrtNm: "Biscuit Factory,Drummond Rd",
              BldgNb: "100",
              PstCd: "SE164DG",
              TwnNm: "LONDON",
              Ctry: "GB"
            }
          },
          DbtrAcct: {
            Id: {
              IBAN: "GB69HBUK40127685143535"
            }
          },
          DbtrAgt: {
            FinInstnId: {
              BIC: "HBUKGB4B",
              PstlAdr: {
                Ctry: "GB"
              }
            }
          },
          ChrgBr: "SLEV",
          CdtTrfTxInf: requestedBatchPayment.payments.map(
            buildCreditorInformation
          )
        }
      }
    }
  };
  const document = builder.create(obj, { encoding: "utf-8" });
  const xmldoc = document.end({ pretty: true });
  return xmldoc;
};
module.exports = generateSepa;
