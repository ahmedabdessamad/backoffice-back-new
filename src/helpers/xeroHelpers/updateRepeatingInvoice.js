const getXeroClient = require("./getXeroClient");

async function updateRepeatingInvoice(
  repeatingInvoiceID,
  tenantId,
  repeatingInvoice
) {
  // console.log("Args = ", repeatingInvoiceID, repeatingInvoice);
  try {
    const xero = await getXeroClient();
    const apiResponse = await xero.accountingApi.updateInvoice(
      tenantId,
      repeatingInvoiceID,
      repeatingInvoice
    );
    // console.log("Invoice %s ", repeatingInvoiceID, apiResponse);
    return apiResponse.body.invoices;
  } catch (e) {
    //console.log('Error details:', e.response.body.Elements[0].ValidationErrors);
    console.log("Error updating invoice:", e);
  }
}
module.exports = updateRepeatingInvoice;
