const schedule = require("node-schedule");
const RandExp = require("randexp");
const getXeroClient = require("./getXeroClient");
const findDocument = require("../findDocument");
const models = require("../../models");
const { tokenSet, Hsbc, Cron } = models;
const BatchPayment = models.batchPayment;

const upsertBatchPayment = (batchPayment) => {
  //console.log("Inserting batch payment ", batchPayment.batchPaymentID);
  //return BatchPayment.create(batchPayment);

  return BatchPayment.findOneAndUpdate(
    { batchPaymentID: batchPayment.batchPaymentID },
    batchPayment,
    {
      upsert: true,
      setDefaultsOnInsert: true
    }
  );
};
const msgID = new RandExp(/^([a-zA-Z0-9 /\-?:().,']{1,35})$/).gen();

const generateHsbcData = (requestedBatchPayment) => {
  console.log("insert hsbc:", requestedBatchPayment.batchPaymentID);
  const obj = {
    batchPaymentID: requestedBatchPayment.batchPaymentID,
    data: {
      paymentInformationId: new RandExp(
        /^([a-zA-Z0-9 /\-?:().,']{1,35})$/
      ).gen(),
      creationDateTime: Date.now(),
      numberOfTransactions: 1,
      initiatingParty: {
        name: "Club Freelance SaS",
        postalAddress: {
          country: "FR",
          addressLine: ["18 rue de la DSP2", "75008 PARIS"]
        },
        organisationId: {
          identification: "c633efcf-e85a-41c1-93b9-8cb4777bd91a",
          schemeName: "COID",
          issuer: "ACPR"
        }
      },
      paymentTypeInformation: {
        serviceLevel: "SEPA",
        localInstrument: "INST",
        categoryPurpose: "DVPM"
      },
      debtor: {
        name: "Club Freelance SaS",
        postalAddress: {
          country: "FR",
          addressLine: ["18 rue de la DSP2", "75003 Paris Ile-de-France"]
        },
        privateId: {
          identification: "c633efcf-e85a-41c1-93b9-8cb4777bd91a",
          schemeName: "BANK",
          issuer: "BICXYYTTZZZ"
        }
      },
      creditor: {
        name: "myMerchant",
        postalAddress: {
          country: "FR",
          addressLine: ["18 rue de la DSP2", "75008 PARIS"]
        },
        organisationId: {
          identification: "852126789",
          schemeName: "SIREN",
          issuer: "FR"
        }
      },
      creditorAccount: { iban: `${requestedBatchPayment.bankAccountNumber}` },
      ultimateCreditor: {
        name: "myPreferedUltimateMerchant",
        postalAddress: {
          country: "FR",
          addressLine: ["18 rue de la DSP2", "75008 PARIS"]
        },
        organisationId: {
          identification: "85212678900025",
          schemeName: "SRET",
          issuer: "FR"
        }
      },
      purpose: "COMC",
      chargeBearer: "SLEV",
      creditTransferTransaction: [
        {
          paymentId: {
            instructionId: new RandExp(
              /^([a-zA-Z0-9 /\-?:().,']{10,35})$/
            ).gen(),
            endToEndId: msgID
          },
          requestedExecutionDate: "2016-12-31T00:00:00.000+01:00",
          instructedAmount: {
            currency: "EUR",
            amount: `${requestedBatchPayment.totalAmount}`
          },
          remittanceInformation: ["MyRemittanceInformation"]
        }
      ],
      supplementaryData: {
        acceptedAuthenticationApproach: ["REDIRECT", "DECOUPLED"],
        successfulReportUrl: `${process.env.HOST}/succ_callback/${requestedBatchPayment.batchPaymentID}`,
        unsuccessfulReportUrl: "/paymentFailure"
      }
    }
  };
  return Hsbc.findOneAndUpdate(
    { batchPaymentID: requestedBatchPayment.batchPaymentID },
    obj,
    {
      upsert: true,
      setDefaultsOnInsert: true
    }
  );
};

const getNewBatchPayments = async (filter, xeroClient) => {
  const tenants = xeroClient.tenants;
  // console.log("tenants=", tenants);

  const xeroBatchPayments = tenants.map(async (tenant) => {
    // console.log("tenant=", tenant);
    const apiResponse = await xeroClient.accountingApi.getBatchPayments(
      tenant.tenantId,
      filter
    );

    apiResponse.body.batchPayments.map((i) => {
      i.organisation = tenant.tenantName;
      i.tenant = tenant.tenantId;
    });

    return apiResponse.body.batchPayments;
  });

  return await Promise.all(xeroBatchPayments);
};

async function getBatchPayments() {
  schedule.scheduleJob(" */5 * * * *", async function () {
    console.log("running every  5 minutes");
    const exists = await findDocument(tokenSet);
    const xero = await getXeroClient();

    if (!xero) return;

    const lastExecution = await Cron.findOne({});

    if (exists) {
      try {
        let newBPayments = [];
        if (lastExecution) {
          let date = new Date(lastExecution.cronDate);
          newBPayments = await getNewBatchPayments(date, xero);
        } else newBPayments = await getNewBatchPayments(null, xero);

        const promises = newBPayments.map(async (bPayment) => {
          await Promise.all(bPayment.map((b) => upsertBatchPayment(b)));
        });
        // console.log("promises=", promises);
        try {
          await Promise.all(promises);

          Cron.findOneAndUpdate(
            {},
            { cronDate: Date.now() },
            { upsert: true },
            (err) => {
              if (err) throw err;
            }
          );
        } catch (e) {
          throw e;
        }

        newBPayments.map(async (bPayment) => {
          await Promise.all(bPayment.map((b) => generateHsbcData(b)));
        });
      } catch (e) {
        console.log(e);
        throw Error(e);
      }
    }
  });
}

module.exports = getBatchPayments;
