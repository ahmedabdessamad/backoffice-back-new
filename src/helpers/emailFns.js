const mailjet = require("node-mailjet").connect(
  process.env.MJ_APIKEY_PUBLIC,
  process.env.MJ_APIKEY_PRIVATE
);
const {
  emailSender,
  templates,
  financeReceiver
} = require("../configs/mailing.conf");
const Account = require("../models/account");
const Client = require("../models/client");
const JobOrder = require("../models/jobOrder");
import runtimeVars from "../configs/runtimeVars";

const { NODE_ENV } = runtimeVars;

const notifyUserOnSignUp = (user, clearPW) => {
  const htmlStr = `
    Hello!<br>
    Your cockpit account has been created. Use these credentials to login: <br>
    email: ${user.email}<br>
    password: ${clearPW}<br><br>
    With love, CF Bot.
  `;

  const messagesPayload = {
    Messages: [
      {
        From: {
          Email: emailSender,
          Name: "[Cockpit] Club Freelance"
        },
        To: [
          {
            Email:
              NODE_ENV === "production" ? user.email : "cockpit@mailinator.com",
            Name: user.username || ""
          }
        ],
        Subject: "Your Account Has Been Created",
        HTMLPart: htmlStr
      }
    ]
  };

  return mailjet.post("send", { version: "v3.1" }).request(messagesPayload);
};

const notifyClientOnSendoutValidation = async (jobSubmission) => {
  const jobOrder = await JobOrder.findOne({ id: jobSubmission.jobOrder.id });
  const client = await Client.findOne({ id: jobOrder.clientContact.id });
  const clientAccount = await Account.findOne({ _id: client.accountId });

  const messagesPayload = {
    Messages: [
      {
        From: {
          Email: emailSender,
          Name: "Club Freelance"
        },
        To: [
          {
            Email:
              NODE_ENV === "production"
                ? client.email
                : "testclient@mailinator.com",
            Name: `${client.firstname} ${client.lastname}`
          }
        ],
        TemplateLanguage: true,
        Subject: "Ce candidat devrait vous intéresser!",
        Variables: {
          firstName: client.firstname,
          emailTo: client.email,
          jobTitle: jobOrder.title
        },
        TemplateID: templates.clientOnSendoutValidation[clientAccount.cfWebsite]
      }
    ]
  };

  return mailjet.post("send", { version: "v3.1" }).request(messagesPayload);
};

const notifyOnNewBatchPayment = (batchPayment) => {
  const sourceCompany = "SAS";
  const htmlStr = `
    Hello!<br>
    A new batch payment has been created and is available for review on your cockpit<br>
    Source company: ${sourceCompany}<br>
    Description: <br>
    Total Amount: ${batchPayment.totalAmount} <br>
    Please make sure all the data is correct before processing payment.<br><br>
    With love, CF Bot.
  `;

  const messagesPayload = {
    Messages: [
      {
        From: {
          Email: emailSender,
          Name: "[Cockpit] Club Freelance"
        },
        To: [
          {
            Email: financeReceiver,
            Name: "Finance Team"
          }
        ],
        Subject: "A new batch payment has been created",
        HTMLPart: htmlStr
      }
    ]
  };

  return mailjet.post("send", { version: "v3.1" }).request(messagesPayload);
};
const notifyUserOnResetPassword = (user, url, method, pwd) => {
  let htmlStr;
  if (method === "login") {
    htmlStr = `
  Hello ${user.username} <br>
  You are receiving this because you (or someone else) have requested the reset of the password for your account. <br>
  kindly use this link to get your new password <br>
  <a href=${url}>${url}</a>
  `;
  }
  if (method === "update") {
    htmlStr = `
    Hello ${user.username} <br>
    your password have been changed for some reasons, this your new password: ${pwd} <br>
    please use it to access the dashboard`;
  }
  const messagesPayload = {
    Messages: [
      {
        From: {
          Email: emailSender,
          Name: "[Cockpit] Club Freelance"
        },
        To: [
          {
            Email: user.email,
            Name: user.username || ""
          }
        ],
        Subject: "Password Reset",
        HTMLPart: htmlStr
      }
    ]
  };
  return mailjet.post("send", { version: "v3.1" }).request(messagesPayload);
};
module.exports = {
  notifyUserOnSignUp,
  notifyClientOnSendoutValidation,
  notifyOnNewBatchPayment,
  notifyUserOnResetPassword
};
