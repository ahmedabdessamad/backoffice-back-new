import { generate } from "generate-password";
import bcrypt from "bcryptjs";
import { sign, verify } from "jsonwebtoken";
import runtimeVars from "../configs/runtimeVars";

const models = require("../models");
const { User, Permission } = models;
const { SECRET_WORD } = runtimeVars;

const randomPassword = () =>
  generate({
    length: 10,
    numbers: true
  });

const encryptPassword = async (password) => {
  const salt = await bcrypt.genSalt(10);
  return bcrypt.hash(password, salt);
};

//check password
const comparedPassword = (password, hash) => bcrypt.compare(password, hash);

//create a token
const getToken = (payload) => sign(payload, SECRET_WORD);

//verifier
const getPayload = async (token) => {
  try {
    //parse the jwt string and store the resut in payload
    const payload = verify(token, SECRET_WORD);
    const user = await User.findById(payload._id)
      .populate("permissions")
      .lean();
    return { loggedIn: true, user };
  } catch (err) {
    return { loggedIn: false };
  }
};

/**
 * Returns if user has the provided group
 * @param user
 * @param group
 * @return {boolean}
 */
const hasGroup = (user, group) => {
  const result = user.permission.filter(
    ({ groupe: { name } }) => name === group
  );
  return !!result.length;
};

module.exports = {
  randomPassword,
  encryptPassword,
  comparedPassword,
  getPayload,
  getToken,
  hasGroup
};
