const axios = require("axios");
const fs = require("fs");
const https = require("https");

const cert = fs.readFileSync("./ssl/qwac.pem", "utf8");
const key = fs.readFileSync("./ssl/hsbc.pem", "utf8");

const getHSBCToken = async () => {
  try {
    const { data } = await axios({
      method: "POST",
      url: "https://sandbox.hsbc.com/psd2/stet/v1.4/token",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      params: {
        grant_type: "client_credentials",
        scope: "pisp",
        client_id: "c633efcf-e85a-41c1-93b9-8cb4777bd91a"
      },
      httpsAgent: new https.Agent({
        cert,
        key,
        rejectUnauthorized: false
      })
    });

    return data;
  } catch (e) {
    throw new Error(`ERROR_REQUESTING_HSBC_TOKEN ${e}`);
  }
};

const paymentRequest = (token, data) =>
  axios({
    method: "POST",
    url: "https://sandbox.hsbc.com/psd2/stet/v1.4/payment-requests",
    headers: {
      Authorization: `Bearer ${token}`,
      Signature: "CCC",
      "X-Request-ID": "DDD",
      "Content-Type": "application/json"
    },
    data: data,

    httpsAgent: new https.Agent({
      cert,
      key,
      rejectUnauthorized: false
    })
  });

const hsbcConnect = async (hsbcData) => {
  const { access_token } = await getHSBCToken();


  try {
    const { data } = await paymentRequest(access_token, hsbcData);
    // console.log('Response =', data);
    return data;
  } catch (e) {
    console.log("ERROR_REQUEST_PAYMENT ", e);
  }
};
module.exports = hsbcConnect;
