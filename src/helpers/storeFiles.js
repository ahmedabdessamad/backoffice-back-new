const { createWriteStream, existsSync, mkdirSync } = require("fs");
const path = require("path");

function storeUpload(stream, filepath) {
  const directorypath = path.dirname(filepath);
  if (!existsSync(directorypath)) {
    mkdirSync(directorypath, { recursive: true });
  }
  return new Promise((resolve, reject) =>
    stream
      .pipe(createWriteStream(filepath))
      .on("finish", () => resolve())
      .on("error", reject)
  );
}
module.exports = {
  storeUpload
};
