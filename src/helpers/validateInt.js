import { INVALID_INPUT } from "./errors";

const validateInt = (value) => {
  const parsed = Number.parseInt(value, 10);
  if (!parsed && parsed !== 0) throw INVALID_INPUT;
  if (value != parsed) throw INVALID_INPUT;
  if (String(value).length !== String(parsed).length) throw INVALID_INPUT;

  return parsed;
};

export default validateInt;
