const models = require("../models");
// const tokenSet = models.tokenSet;
// const token = require('../configs/token.json');
const { Role, Group } = models;

const ROLES = ["Admin", "Reader", "Editor"];
const GROUPS = [
  "Finance",
  "Data",
  "HR",
  "Marketing",
  "Sales",
  "Comex",
  "Legal",
  "Sourcing",
  "SuperAdmin"
];

const initial = (name, collection) => {
  collection.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new collection({
        name: name
      }).save((err) => {
        if (err) return console.log("error", err);
        // console.log("added names to ", collection);
      });
    }
  });
};
const initialCollections = () => {
  for (const r of ROLES) initial(r, Role);
  for (const g of GROUPS) initial(g, Group);
};
/* const initTokenSet = () => {
  tokenSet.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      const newtoken = new tokenSet(token);
      newtoken.save(function (err, token) {
        if (err) return console.error(err);
        console.log('saved to tokenSet collection.');
      });
    }
  });
}; */

module.exports = { initialCollections };
