const INVALID_INPUT = new Error("INVALID_INPUT"); // missing or invalid input 422
INVALID_INPUT.status = 422;

const NOT_FOUND = new Error("NOT_FOUND"); // the requested entity not found
NOT_FOUND.status = 404;

const NOT_ATHORIZED = new Error("NOT_AUTHORIZED"); // the requested entity not found
NOT_ATHORIZED.status = 401;

const NOT_FOUND_OR_UNAUTHORIZED = new Error("NOT_FOUND_OR_UNAUTHORIZED");
NOT_FOUND_OR_UNAUTHORIZED.status = 403;

const SERVER_ERROR = new Error("SERVER_ERROR"); // internal error: db failure or server down
SERVER_ERROR.status = 500;

const NOT_AUTHENTICATED = new Error("NOT AUTHENTICATED");
NOT_AUTHENTICATED.status = 401;

const ALREADY_PROCESSED = new Error("ALREADY_PROCESSED");
ALREADY_PROCESSED.status = 208;

export {
  NOT_FOUND,
  INVALID_INPUT,
  SERVER_ERROR,
  NOT_ATHORIZED,
  NOT_FOUND_OR_UNAUTHORIZED,
  ALREADY_PROCESSED,
  NOT_AUTHENTICATED
};
