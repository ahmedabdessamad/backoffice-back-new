import { verify } from "jsonwebtoken";
import runtimeVars from "../configs/runtimeVars";

const { SECRET_WORD } = runtimeVars;

const decode = (token) => {
  // authentication required
  if (!token) throw new Error("ACCESS DENIED");

  // extract data from the passed json
  try {
    return verify(token, SECRET_WORD);
  } catch (err) {
    throw new Error("ACCESS DENIED");
  }
};

module.exports = decode;
