import bhGrabber from "../bhGrabber";

const NOT_FOUND = new Error("NOT_FOUND");

const getJobOrderFromBH = async (id, fields) => {
  if (!id) return null;
  if (!fields) throw new Error("Fields not sated");
  const jobOrder = await bhGrabber("JobOrder", id, fields);
  if (!jobOrder) throw NOT_FOUND;
  return jobOrder;
};

export default getJobOrderFromBH;
