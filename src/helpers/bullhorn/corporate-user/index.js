import bhGrabber from "../bhGrabber";
const NOT_FOUND = new Error("NOT_FOUND");

const getCorporateUserFromBH = async (id, fields) => {
  if (!id) return null;
  if(!fields) fields = ["name", "id"];
  const corporateUser = await bhGrabber("CorporateUser", id, fields);

  if (!corporateUser) throw NOT_FOUND;
  return corporateUser;
};

export default getCorporateUserFromBH;
