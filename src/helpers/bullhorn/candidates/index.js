import bhGrabber from "../bhGrabber";

const NOT_FOUND = new Error("NOT_FOUND");

const getCandidateFromBH = async (id, fields) => {
  if (!id) return null;
  if (!fields) fields = ["name", "id"];
  const candidate = await bhGrabber("Candidate", id, fields);
  if (!candidate) throw NOT_FOUND;
  return candidate;
};

export default getCandidateFromBH;
