import { RestSEssionApi, EntitiesApi } from "bh-sdk";
import runtimeVars from "../../configs/runtimeVars";

const { BH } = runtimeVars;

const bhClient = async () => {
  const client = new RestSEssionApi({
    clientId: BH.CLIENT_ID,
    clientSecret: BH.CLIENT_SECRET,
    username: BH.USERNAME,
    password: BH.PASSWORD,
    restSessionMinutesToLive: BH.TTL
  });
  return await new Promise((resolve, reject) => {
    client.createRestSession((err) => {
      if (err) return reject(err);
      return resolve(new EntitiesApi(client));
    });
  });
};

export default bhClient();
