import bhGrabber from "../bhGrabber";

const NOT_FOUND = new Error("NOT_FOUND");

const getClientFromBH = async (id, fields) => {
  if (!id) return null;
  if(!fields) fields = ["name", "id"];
  const client = await bhGrabber("ClientContact", id, fields);
  if (!client) throw NOT_FOUND;
  return client;
};

export default getClientFromBH;
