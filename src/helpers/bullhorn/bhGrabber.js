import bhClient from "./bhClient";
import runtimeVars from "../../configs/runtimeVars";

const bhGrabber = async (entity, id, fields) => {
  if (!fields) throw new Error("FIELDS NOT FOUND");
  const client = await bhClient;
  let tmr = false;
  let i = 0;
  let bhEntity = null;
  do {
    tmr = false;
    try {
      bhEntity = await new Promise((resolve, reject) => {
        client.findEntity(entity, id, fields || ["*"], (err, res) => {
          if (err) {
            if (err.response && err.response.status === 429) {
              console.log("TMR bhGrabber i = ", i);
              return reject(err);
            }
            return reject(err);
          }
          return resolve(res && res.data);
        });
      });
    } catch (e) {
      if (e.response.status === 429) {
        tmr = true;
        i += 1;
        await new Promise((r) => setTimeout(r, runtimeVars.TIME_OUT));
      } else {
        throw e;
      }
    }
  } while (i < runtimeVars.NUMBER_OF_RETRY && tmr === true);
  if (!bhEntity) {
    throw new Error("Too Many Requests > 100");
  }
  return bhEntity;
};

export default bhGrabber;
