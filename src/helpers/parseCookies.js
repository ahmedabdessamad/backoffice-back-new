const parseCookies = (cookies) => {
  try {
    let list = {},
      rc = cookies;

    rc &&
      rc.split(";").forEach(function (cookie) {
        let parts = cookie.split("=");
        list[parts.shift().trim()] = decodeURI(parts.join("="));
      });

    return list;
  } catch (e) {
    return null;
  }
};

export default parseCookies;
