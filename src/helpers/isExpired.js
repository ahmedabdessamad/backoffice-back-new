const models = require("../models");
const tokenSet = models.tokenSet;

const isExpired = async () => {
  let token = null;
  try {
    token = await tokenSet.findOne();
  } catch (e) {
    console.error(e);
    return false;
  }
  if (!token) return false;

  const expiresAt = token.expires_at;
  return Math.floor(Date.now() / 1000) >= expiresAt;
};
module.exports = isExpired;
