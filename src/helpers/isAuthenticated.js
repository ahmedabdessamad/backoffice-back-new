const { AuthenticationError } = require("apollo-server-express");

const isAuthenticated = (ctx) => {
  if (!ctx || !ctx.user) throw new AuthenticationError("UNAUTHENTICATED");

  return ctx.user;
};

module.exports = isAuthenticated;
