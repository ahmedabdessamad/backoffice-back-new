import syncLatestItems from "../helpers/xeroHelpers/items/syncLatestItems";
import createItems from "../helpers/xeroHelpers/items/createItems";

const xeroItemSynchro = async () => {
  console.info("running every  10 minutes ");

  // sync SAS items
  try {
    await syncLatestItems("SAS");
    console.info("%s SAS Items synched", new Date());
  } catch (err) {
    console.warn("%s Error synching SAS items: %s", new Date(), err);
  }

  // sync LTD items
  try {
    await syncLatestItems("LTD");
    console.info("%s LTD Items synched", new Date());
  } catch (err) {
    console.warn("%s Error synching LTD items: %s", new Date(), err);
  }

  //Create New Items
  try {
    await createItems();
    console.info("%s Items are pushed", new Date());
  } catch (err) {
    console.warn("%s Error synching push items: %s", new Date(), err);
  }
};

export default xeroItemSynchro;
