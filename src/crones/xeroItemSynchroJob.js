import xeroItemSynchro from "./xeroItemSynchro";
import schedule from "node-schedule";

async function xeroItemSynchroJob() {
  schedule.scheduleJob(" */10 * * * *", async function () {
    console.log("hear");
    xeroItemSynchro()
      .then(() => {
        console.info("%s all items are pulled", new Date());
      })
      .catch((e) => {
        console.warn("%s Error synching items: %s", new Date(), e);
      });
  });
}

export default xeroItemSynchroJob;
