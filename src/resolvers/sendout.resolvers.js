import bhEntitySvc from "../helpers/bullhorn/bhClient";
import runtimeVars from "../configs/runtimeVars";
import { response } from "express";
const path = require("path");
const fs = require("fs");
const { AuthenticationError } = require("apollo-server-express");
const { JobSubmission, ValidatedJobSubmission } = require("../models");
const { notifyClientOnSendoutValidation } = require("../helpers/emailFns");
const { storeUpload } = require("../helpers/storeFiles");
const isAuthenticated = require("../helpers/isAuthenticated");
const { checkPermissionAndGetRole } = require("../helpers/authorization");
const fetchCandidateFromBH = async (candidateID, bhEntitySvc) => {
  const { data } = await bhEntitySvc.findEntityAsync("Candidate", candidateID, [
    "id",
    "firstName",
    "lastName",
    "occupation",
    "dateAvailable",
    "address"
  ]);

  return data;
};

const fetchCandidateFilesFromBH = async (candidateID, bhEntitySvc) => {
  let tmr = false;
  let i = 0;
  let bhEntity = null;
  do {
    tmr = false;
    try {
      bhEntity = await new Promise((resolve, reject) => {
        bhEntitySvc.getAssociation(
          "Candidate",
          candidateID,
          "fileAttachments",
          ["id", "name", "type"],
          (err, result) => {
            if (err) {
              if (err.response && err.response.status === 429) {
                console.log("TMR fetchCandidateFilesFromBH i = ", i);
              }
              return reject(err);
            }
            return resolve(result);
          }
        );
      });
    } catch (e) {
      if (e.response.status === 429) {
        tmr = true;
        i += 1;
        await new Promise((r) => setTimeout(r, runtimeVars.TIME_OUT));
      } else {
        throw e;
      }
    }
  } while (i < runtimeVars.NUMBER_OF_RETRY && tmr === true);
  if (!bhEntity) {
    throw new Error("Too Many Requests > 100");
  }
  return bhEntity;
};
const fetchCandidateReferencesFromBH = async (candidateID, bhEntitySvc) => {
  const { data } = await bhEntitySvc.queryAsync(
    "CandidateReference",
    `isDeleted=false AND candidate.id=${candidateID}`,
    [
      "id",
      "referenceFirstName",
      "referenceLastName",
      "referenceTitle",
      "customTextBlock1"
    ]
  );

  return data;
};

const getBHRestSession = (bhEntitySvc) => {
  return bhEntitySvc.restSession;
};

const downloadBHFile = async (candidateID, candidateFile, bhEntitySvc) => {
  const { id, name } = candidateFile;

  const fileName = `${Date.now()}-${name.replace(/\s/g, "")}`;
  const filePath = `./files/cv/${fileName}`;
  const dirPath = path.dirname(filePath);

  try {
    if (!fs.existsSync(dirPath)) {
      fs.mkdirSync(dirPath, { recursive: true });
    }
  } catch (e) {
    throw new Error(`ERROR_CREATING_DIR: ${e}`);
  }

  let tmr = false;
  let i = 0;
  let bhEntity = null;
  do {
    tmr = false;
    try {
      bhEntity = await new Promise((resolve, reject) => {
        bhEntitySvc.getFile(
          "Candidate",
          candidateID,
          id,
          false,
          (err, result) => {
            if (err) {
              if (err.response && err.response.status === 429) {
                console.warn("TMR downloadBHFile i = ", i);
              }
              return reject(err);
            }
            fs.writeFile(
              filePath,
              result.File.fileContent,
              { encoding: "base64" },
              function (err) {
                if (err) return reject(err);
                return resolve(filePath);
              }
            );
          }
        );
      });
    } catch (e) {
      if (e.response.status === 429) {
        tmr = true;
        i += 1;
        await new Promise((r) => setTimeout(r, runtimeVars.TIME_OUT));
      } else {
        throw e;
      }
    }
  } while (i < runtimeVars.NUMBER_OF_RETRY && tmr === true);
  if (!bhEntity) {
    throw new Error("Too Many Requests > 10");
  }
  return bhEntity;
};

const uploadBHFile = async (candidateID, file, fileName, bhEntitySvc) => {
  let tmr = false;
  let i = 0;
  let bhEntity = null;
  do {
    tmr = false;
    try {
      bhEntity = await new Promise((resolve, reject) => {
        const fd = fs.createReadStream(file);
        bhEntitySvc.addFileRaw(
          "Candidate",
          candidateID,
          fd,
          "SAMPLE",
          fileName,
          {},
          function (err, result) {
            if (err) {
              if (err.response && err.response.status === 429) {
                console.warn("ERR BH UPLOAD TMR i = ", i);
              } else {
                console.warn("ERR BH UPLOAD ", err);
              }
              return reject(err);
            }
            return resolve(result);
          }
        );
      });
    } catch (e) {
      if (e.response.status === 429) {
        tmr = true;
        i += 1;
        await new Promise((r) => setTimeout(r, runtimeVars.TIME_OUT));
      } else {
        throw e;
      }
    }
  } while (i < runtimeVars.NUMBER_OF_RETRY && tmr === true);
  if (!bhEntity) {
    throw new Error("Too Many Requests > 10");
  }
  return bhEntity;
};

const sendoutResolvers = {
  Query: {
    sendout: async (parent, args, context) => {
      const user = isAuthenticated(context);
      const { id } = args;

      let filter = { id };
      /*if (hasGroup(user, "Sales")) {
        filter = { ...filter, "owners.data.id": user.bhID };
      }*/
      //filter = { ...filter, "owners.data.id": user.bhID };

      // get jobSubmission
      const jobSubmission = await JobSubmission.findOne(filter);

      if (!jobSubmission) {
        throw new AuthenticationError("NOT_AUTHORIZED");
      }
      // fetch candidate from BH
      const candidate = await fetchCandidateFromBH(
        jobSubmission.candidate.id,
        await bhEntitySvc
      );

      return {
        id: jobSubmission.id,
        jobOrderID: jobSubmission.jobOrder.id,
        jobOrderTitle: jobSubmission.jobOrder.title,
        candidateID: candidate.id,
        candidateFirstName: candidate.firstName,
        candidateLastName: candidate.lastName,
        candidateOccupation: candidate.occupation,
        candidateAvailableDate: candidate.dateAvailable,
        candidateAddress: candidate.address,
        comments: jobSubmission.comments,
        payRate: jobSubmission.payRate,
        status: jobSubmission.validationStatus
      };
    },
    sendouts: async (parent, args, context) => {
      const user = isAuthenticated(context);
      const { validationStatus, limit, skip } = args;
      const userRole = checkPermissionAndGetRole(
        { PENDINGSENDOUTS: 6, SENDOUTS: 6, ARCHIVEDSENDOUTS: 6 },
        user
      );

      let filter = { validationStatus: { $in: validationStatus } };
      console.log("userRole", userRole);
      if (userRole === "Editor") {
        filter = { ...filter, "owners.data.id": user.bhID };
        console.log("filter",filter);
      }

      const total = await JobSubmission.find(filter).count();

      const jobSubmissions = await JobSubmission.find(filter)
        .skip(skip)
        .limit(limit)
        .sort({
          id: -1
        });
      console.log("jobSubmissions", jobSubmissions);
      return jobSubmissions.map(async (jobSubmission) => {
        const candidate = await fetchCandidateFromBH(
          jobSubmission.candidate.id,
          await bhEntitySvc
        );

        return {
          id: jobSubmission.id,
          jobOrderID: jobSubmission.jobOrder.id,
          jobOrderTitle: jobSubmission.jobOrder.title,
          candidateID: candidate.id,
          candidateFirstName: candidate.firstName,
          candidateLastName: candidate.lastName,
          candidateOccupation: candidate.occupation,
          candidateAvailableDate: candidate.dateAvailable,
          candidateAddress: candidate.address,
          comments: jobSubmission.comments,
          payRate: jobSubmission.payRate,
          status: jobSubmission.validationStatus,
          total
        };
      });
    }
  },
  Mutation: {
    validateSendout: async (parent, args) => {
      const { input } = args;

      // save the file
      try {
        var candidateCv = await downloadBHFile(
          input.candidateID,
          input.candidateFile,
          await bhEntitySvc
        );
      } catch (e) {
        throw new Error(`ERROR_DOWNLOADING_CV_FILE: ${e}`);
      }

      // create a validatedJobSubmission
      const newValidSendout = {
        ...input,
        status: "cv sent",
        candidateCv
      };

      try {
        const validSendout = await ValidatedJobSubmission.findOneAndUpdate(
          { jobSubmissionID: input.jobSubmissionID },
          newValidSendout,
          { new: true, upsert: true }
        );
        // NB: The returned jobSubmission MUST BE THE PRE UPDATE document
        const jobSubmission = await JobSubmission.findOneAndUpdate(
          { id: input.jobSubmissionID },
          { validationStatus: "validated" }
        );
        if (jobSubmission.validationStatus === "pending") {
          notifyClientOnSendoutValidation(jobSubmission);
        }
        return validSendout;
      } catch (e) {
        throw new Error(`ERROR_VALIDATING_SENDOUT: ${e}`);
      }
    },
    addCandidateFile: async (parent, args) => {
      const { candidateID, file } = args;
      const { createReadStream, filename } = await file;
      const stream = createReadStream();

      try {
        const filePath = path.resolve(
          __dirname + `./../../files/cv/${Date.now()}-${filename}`
        );

        await storeUpload(stream, filePath);
        await uploadBHFile(candidateID, filePath, filename, await bhEntitySvc);
        fs.unlink(filePath, () => {});
        return "SUCCESS_UPLOAD_FILE";
      } catch (e) {
        console.log("Error = ", e)
        throw new Error("ERR_UPLOAD_FILE");
      }
    },
    rejectSendout: async (parent, args) => {
      const { id } = args;
      // get jobSubmission
      const jobSubmission = await JobSubmission.findOneAndUpdate(
        { id: id },
        { validationStatus: "rejected" }
      );

      return {
        id: jobSubmission.id,
        jobOrderID: jobSubmission.jobOrder.id,
        jobOrderTitle: jobSubmission.jobOrder.title,
        comments: jobSubmission.comments,
        payRate: jobSubmission.payRate,
        status: jobSubmission.validationStatus
      };
    }
  },
  Sendout: {
    candidateFiles: async (parent) => {
      try {
        const { restUrl, bhRestToken } = getBHRestSession(await bhEntitySvc);
        const { data: files } = await fetchCandidateFilesFromBH(
          parent.candidateID,
          await bhEntitySvc
        );
        return files.map((file) => ({
          ...file,
          url: `${restUrl}file/Candidate/${parent.candidateID}/${file.id}/raw?BhRestToken=${bhRestToken}`
        }));
      } catch (e) {
        console.log("ERROR_RESOLVING_CANDIDATE_FILES: ", e);
        return [];
      }
    },
    reference: async (parent) => {
      try {
        const references = await fetchCandidateReferencesFromBH(
          parent.candidateID,
          await bhEntitySvc
        );
        return references.map((reference) => ({
          ...reference,
          outcome: reference.customTextBlock1
        }));
      } catch (e) {
        console.log("ERROR_RESOLVING_CANDIDATE_REFERENCES: ", e);
        return [];
      }
    }
  }
};

module.exports = sendoutResolvers;
