import Item from "../../models/item";

const itemQuery = {
  async items() {
    const result = await Item.find().populate("placement");
    return result;
  },

  async item(parent, args) {
    if (args.itemID) {
      const result = await Item.findOne({ itemID: args.itemID }).populate(
        "placement"
      );
      return result.toObject();
    }
    if (args.code) {
      const result = await Item.findOne({ code: args.code }).populate(
        "placement"
      );
      return result.toObject();
    }
  }
};

export default itemQuery;
