import Item from "../../models/item";

const itemMutation = {
  async createItem(parent, args) {
    const newItem = new Item(args.data);
    await newItem.save((err) => {
      if (err) throw new Error(err);
    });
    return newItem;
  },

  async deleteItem(parent, args) {
    const { itemID } = args;

    const deletedItem = await Item.findOneAndDelete({ itemID });

    if (!deletedItem) {
      throw new Error(`Item Not Found`);
    }
    return deletedItem;
  }
};

export default itemMutation;
