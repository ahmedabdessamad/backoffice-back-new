import itemQuery from "./itemQuery";
import itemMutation from "./itemMutation";
import itemResolver from "./itemResolver";

const item = {
  Query: itemQuery,
  Mutation: itemMutation,
  Item: itemResolver
};

module.exports = item;
