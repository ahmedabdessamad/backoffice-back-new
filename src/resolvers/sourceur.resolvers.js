const models = require("../models");
const Sourceur = models.Sourceur;
const sourceurResolvers = {
  Query: {},
  Mutation: {
    addSourceur: async (parent, { email, userID }) => {
      console.log("In add sourcer mutation");
      try {
        //check conditions
        if (!email || !userID) {
          throw new Error("invalid Sourceur");
        }
        const newSourceur = {
          email: email,
          userID: userID
        };
        //create Sourceur
        const sourceur = await Sourceur.create(newSourceur);
        console.log("sourceur registred successfully ");
        return {
          ...sourceur.toObject()
        };
      } catch (error) {
        throw new Error(error);
      }
    }
  },
  Sourceur: {
    sourceurID: (parent) => {
      return parent._id;
    }
  }
};

module.exports = sourceurResolvers;
