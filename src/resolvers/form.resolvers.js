const checkPermission = require("../helpers/authorization/checkPermission");
const isAuthenticated = require("../helpers/isAuthenticated");
const {
  get_forms,
  get_form,
  get_form2,
  createForm,
  updateForm,
  formSending
} = require("./formHelpers");

const formResolvers = {
  Query: {
    forms: async (parent, args, context) => {
     // const user = isAuthenticated(context);
      //checkPermission({ FORM: 4 }, user);
      return await get_forms();
    },
    form: async (parent, args, context) => {
    //  const user = isAuthenticated(context);
     // console.log("user", user);
      console.log("args1",args);
      return await get_form(args);
    },
    form2: async (parent, args, context) => {
      //const user = isAuthenticated(context);
     // console.log("user", user);
      console.log("args",args);
      return await get_form2(args);
    }
  },
  Mutation: {
    addForm: async (parent, args, context) => {
      const user = isAuthenticated(context);
     // checkPermission({ FORM: 6 }, user);
      return await createForm(args, user);
    },
    editForm: async (parent, args, context) => {
    //  const user = isAuthenticated(context);
     // checkPermission({ FORM: 6 }, user);
      return await updateForm(args);
    },
    sendForm: async (parent, args, context) => {
     // const user = isAuthenticated(context);
     // checkPermission({ FORM: 6 }, user);
      return await formSending(args);
    }
  },
  Form: {
    formID: (parent) => {
      return parent._id;
    }
  },
  User: {
    userID: (parent) => {
      return parent._id;
    }
  }
};

module.exports = formResolvers;
