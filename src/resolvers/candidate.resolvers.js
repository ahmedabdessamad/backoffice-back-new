const models = require("../models");
const Candidate = models.Candidate;

const candidateResolvers = {
  Query: {},
  Mutation: {
    addCandidate: async (email, userID) => {
      try {
        //check conditions
        if (!email || !userID) throw new Error("invalid Candidate");

        const newCandidate = {
          email: email,
          userIDID: userID
        };

        //create Candidate
        return await Candidate.create(newCandidate);
      } catch (error) {
        throw new Error(error);
      }
    }
  },
  Candidate: {
    candidateID: (parent) => parent._id
  }
};

module.exports = candidateResolvers;
