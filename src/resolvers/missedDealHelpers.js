import { getCandidateFromBH } from "../helpers/bullhorn";

const mongoose = require("mongoose");
const models = require("../models");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const fs = require("fs");
const {
  MissedDeal,
  User,
  DataManager,
  MarketingManager,
  ClientCorporation
} = models;
const mailjet = require("node-mailjet");
import { bhEntitySvc } from "../helpers/bullhorn/bhSession";
import runtimeVars from "../configs/runtimeVars";
const CategoryResolvers = require("./Category.resolvers");

const updateCandidateBullhorn = async (
  candidateID,
  toUpdate,
  entitySvc,
  skills
) => {
  if (toUpdate.length !== 0) {
    const candidate = {
      id: candidateID
    };

    for (let i = 0; i < toUpdate.length; i++) {
      if ("dailyRate" in toUpdate[i]) {
        candidate.dayRateLow = toUpdate[i].dailyRate;
      }
      if ("jobTitle" in toUpdate[i]) {
        candidate.occupation = toUpdate[i].jobTitle;
      }
      if ("skills" in toUpdate[i]) {
        const result = await getCandidateFromBH(candidateID, ["primarySkills"]);
        const toDisassociateIds = result.data.primarySkills.data.map(
          (skill) => {
            return skill.id;
          }
        );

        const doDisassociates = async () => {
          if (!toDisassociateIds.length) return;
          try {
            let tmr = false;
            let i = 0;
            do {
              tmr = false;
              try {
                await new Promise((resolve, reject) => {
                  entitySvc.disassociates(
                    "Candidate",
                    candidateID,
                    "primarySkills",
                    toDisassociateIds,
                    (result, error) => {
                      error ? reject(error) : resolve(result);
                    }
                  );
                });
              } catch (e) {
                if (e.response.status === 429) {
                  tmr = true;
                  i += 1;
                  await new Promise((r) => setTimeout(r, runtimeVars.TIME_OUT));
                } else {
                  throw e;
                }
              }
            } while (i < runtimeVars.NUMBER_OF_RETRY && tmr === true);
          } catch (err) {
            console.log(err);
          }
        };

        await doDisassociates();

        const toAssociate = skills.map((skill) => {
          return skill.id;
        });
        console.log("skill to addddd ", toAssociate);
        const doAssociates = async () => {
          try {
            let tmr = false;
            let i = 0;
            let associate = null;
            do {
              tmr = false;
              try {
                associate = await new Promise((resolve, reject) => {
                  entitySvc.createAssociation(
                    "Candidate",
                    candidateID,
                    "primarySkills",
                    toAssociate,
                    (result, error) => {
                      error ? reject(error) : resolve(result);
                    }
                  );
                });
              } catch (e) {
                if (e.response.status === 429) {
                  tmr = true;
                  i += 1;
                  await new Promise((r) => setTimeout(r, runtimeVars.TIME_OUT));
                } else {
                  throw e;
                }
              }
            } while (i < runtimeVars.NUMBER_OF_RETRY && tmr === true);
            console.log("associate", associate);
          } catch (err) {
            console.log(err);
          }
        };
        await doAssociates();
      }
      if ("missionEnd" in toUpdate[i]) {
        candidate.status = "Not Available";
        // add 1 day to the mission end day
        let addday = new Date(toUpdate[i].missionEnd);
        addday.setDate(addday.getDate() + 1);
        let missionEndSplit = addday.toISOString().split("T");
        // to Timestamp
        let d = Math.round(new Date(missionEndSplit[0]).getTime());
        candidate.dateAvailable = d;
        // changing Last availability update
        const updateDate = new Date();
        let updateDateSplit = updateDate.toISOString().split("T");
        // to Timestamps
        let updated = Math.round(new Date(updateDateSplit[0]).getTime());
        candidate.customDate1 = updated;
      }
      if ("job" in toUpdate[i]) {
        const missionStartSplit = toUpdate[i].job.missionStart.split("T");
        // to Timestamps
        let missionStartTimestamps = Math.round(
          new Date(missionStartSplit[0]).getTime()
        );
        const missionEndSplit = toUpdate[i].job.missionEnd.split("T");
        // to Timestamps
        let missionEndTimestamps = Math.round(
          new Date(missionEndSplit[0]).getTime()
        );

        const CandidateWorkHistory = {
          candidate: { id: candidateID },
          companyName: toUpdate[i].job.company,
          startDate: missionStartTimestamps,
          endDate: missionEndTimestamps,
          title: toUpdate[i].job.jobTitle,
          salary2: toUpdate[i].job.dailyRate,
          salaryType: "Daily"
        };
        let tmr = false;
        let i = 0;
        do {
          tmr = false;
          try {
            await new Promise((resolve, reject) => {
              entitySvc.insertEntity(
                "CandidateWorkHistory",
                CandidateWorkHistory,
                (err, result) => {
                  if (err) {
                    if (err.response && err.response.status === 429) {
                      console.log(
                        "Error TMR Insert CandidateWorkHistory i: ",
                        i
                      );
                    } else {
                      console.log("Error: ", err);
                    }
                    return reject(err);
                  }
                  console.log("Result: ", result);
                  return resolve(result);
                }
              );
            });
          } catch (e) {
            if (e.response.status === 429) {
              tmr = true;
              i += 1;
              await new Promise((r) => setTimeout(r, runtimeVars.TIME_OUT));
            } else {
              throw e;
            }
          }
        } while (i < runtimeVars.NUMBER_OF_RETRY && tmr === true);
      }
    }
    let tmr1 = false;
    let i1 = 0;
    let bhEntity1 = null;
    do {
      tmr1 = false;
      try {
        bhEntity1 = await new Promise((resolve, reject) => {
          entitySvc.updateEntity("Candidate", candidate, (err, result) => {
            if (err) {
              if (err.response && err.response.status === 429) {
                console.warn("Error: update candidate i = ", i1);
              } else {
                console.warn("Error: ", err);
              }
              return reject(err);
            }
            console.info("Result: ", result);
            return resolve(result);
          });
        });
      } catch (e) {
        if (e.response.status === 429) {
          tmr1 = true;
          i1 += 1;
          await new Promise((r) => setTimeout(r, runtimeVars.TIME_OUT));
        } else {
          throw e;
        }
      }
    } while (i1 < runtimeVars.NUMBER_OF_RETRY && tmr1 === true);
    console.info("bh entity Candidate", bhEntity1);
  } else return null;
};
//(async function(){updateCandidateBullhorn(79638,[{skills:"dev"}],await bhEntitySvc);})()
/*const getCandidateBullhorn = (candidateID, toUpdate, entitySvc) =>
{


    // send the updated candidate
    entitySvc.findEntity
    (
         "Candidate",
         79638,
         ["id","email","name","occupation","dayRateLow", "status"," dateAvailable","customDate1","workHistories","primarySkills"],
      (err, result, response) => {
         if (err) {
        return console.log("Error: ", err);
        }
        return console.log("Result: ", result.data.workHistories.data);
        }
    );



};*/
//(async function() {getCandidateBullhorn(null, null, await bhEntitySvc)})()
const get_missedDeals = async () => {
  try {
    let missedDeals = await MissedDeal.find();
    missedDeals = missedDeals.reverse();
    return missedDeals.map((missedDeal) => {
      return {
        ...missedDeal._doc,
        _id: missedDeal.id,
        missionStart: new Date(missedDeal._doc.missionStart).toISOString(),
        missionEnd: new Date(missedDeal._doc.missionEnd).toISOString(),
        createdAt: new Date(missedDeal._doc.createdAt).toISOString(),
        creator: User.findById(missedDeal._doc.creator)
        //company: ClientCorporation.findById(missedDeal._doc.company),
        // candidate: Candidate.findById(missedDeal._doc.candidate)
      };
    });
  } catch (error) {
    throw new Error(error);
  }
};

const get_missedDeal = async (args) => {
  try {
    const missedDeal = await MissedDeal.findById(args.missedDealID);
    if (missedDeal) {
      return {
        ...missedDeal._doc,
        _id: missedDeal.id,
        missionStart: new Date(missedDeal._doc.missionStart).toISOString(),
        missionEnd: new Date(missedDeal._doc.missionEnd).toISOString(),
        createdAt: new Date(missedDeal._doc.createdAt).toISOString(),
        creator: User.findById(missedDeal._doc.creator)
        //company: ClientCorporation.findById(missedDeal._doc.company),
        //candidate: Candidate.findById(missedDeal._doc.candidate)
      };
    } else {
      throw new Error("missed deal do not exist!");
    }
  } catch (error) {
    throw new Error(error);
  }
};
const createMissedDeal = async (args, user) => {
  try {
    const creator = await User.findOne({
      _id: new mongoose.Types.ObjectId(user._id)
    }).lean();

    let comp1 = {};
    let compId = "0";
    const company = await ClientCorporation.findOne({
      name: args.input.company
    }).lean();
    if (company) {
      comp1 = company.name;
      compId = company.id;
    } else {
      comp1 = args.input.company;
    }

    const allSkills = await CategoryResolvers.Query.skills();
    const exist = (skill) => {
      for (let i = 0; i < allSkills.length; i += 1) {
        if (allSkills[i].name === skill.name) {
          return allSkills[i];
        }
      }
      return null;
    };

    const skills = args.input.skills.map((skill) => {
      if (exist(skill) !== null) {
        return exist(skill);
      }
    });
    await updateCandidateBullhorn(
      args.input.candidateID,
      args.toUpdate,
      await bhEntitySvc,
      skills
    );

    const missedDeal = new MissedDeal({
      companyId: compId,
      companyName: comp1,
      ITConsultingFirm: args.input.ITConsultingFirm,
      intermedianryAgency: args.input.intermedianryAgency,
      missionStart: new Date(args.input.missionStart),
      missionEnd: new Date(args.input.missionEnd),

      skills: skills,
      jobTitle: args.input.jobTitle,
      dailyRate: args.input.dailyRate,
      city: args.input.city,
      missionProjectDescription: args.input.missionProjectDescription,
      creator: creator,
      candidate: args.input.candidateID
    });

    missedDeal.save();

    /* creator.createdMissedDeals.push(missedDeal);
    creator.save();*/

    const request = mailjet
      .connect(process.env.MJ_APIKEY_PUBLIC, process.env.MJ_APIKEY_PRIVATE)
      .post("send", { version: "v3.1" })
      .request({
        Messages: [
          {
            From: {
              Email: "no-reply@club-freelance.com"
            },
            To: [
              {
                Email: "flemaignent@club-freelance.com"
              }
            ],
            Subject: "A missed deal has just been added",
            TextPart: "",
            HTMLPart: `<a>The missed deal is about the Company: ${
              missedDeal.companyName
            }</a>
              </br>
              </br>
              </br>

              <a>It has been provided by the candidate ID : ${
                missedDeal.candidate
              }</a>
              </br>
              </br>

              He/She is working has a : ${missedDeal.jobTitle}
              </br>
              City : ${missedDeal.city}
              </br>
              Skills: ${missedDeal.skills.map((skill) => skill.name)}
              </br>
              Mission Start: ${missedDeal.missionStart}
              </br>

              Mission End: ${missedDeal.missionEnd}
              </br>

              Description of the mission:
              ${missedDeal.missionProjectDescription} `,
            CustomID: "AppGettingStartedTest"
          }
        ]
      });
    request
      .then((result) => {
        console.log(result.body);
      })
      .catch((err) => {
        console.log(err.statusCode);
      });

    // console.log('missedDeal added successfully ', missedDeal.company);
    // return missedDeal;
    return {
      ...missedDeal._doc,
      _id: missedDeal.id,
      missionStart: new Date(missedDeal._doc.missionStart).toISOString(),
      missionEnd: new Date(missedDeal._doc.missionEnd).toISOString(),
      creator: User.findById(missedDeal._doc.creator)
      // candidate: Candidate.findById(missedDealtobeupdated._doc.candidate)
    };
  } catch (error) {
    throw new Error(error);
  }
};

const updateMissedDeal = async (args) => {
  try {
    const candidate = args.input.candidateID;

    let missedDealtobeupdated = await MissedDeal.findById(args.missedDealID);
    const allSkills = await CategoryResolvers.Query.skills();
    const exist = (skill) => {
      for (let i = 0; i < allSkills.length; i += 1) {
        if (allSkills[i].name === skill.name) {
          return allSkills[i];
        }
      }
      return null;
    };

    const skills = args.input.skills.map((skill) => {
      if (exist(skill)) {
        return exist(skill);
      }
    });

    let comp1 = {};
    let compId = "0";
    const company = await ClientCorporation.findOne({
      name: args.input.company
    }).lean();
    if (company) {
      comp1 = company.name;
      compId = company.id;
    } else {
      comp1 = args.input.company;
    }
    await updateCandidateBullhorn(
      args.input.candidateID,
      args.toUpdate,
      await bhEntitySvc,
      skills
    );
    // add missedDeal
    missedDealtobeupdated.companyId = compId;
    missedDealtobeupdated.companyName = comp1;
    missedDealtobeupdated.ITConsultingFirm = args.input.ITConsultingFirm;
    missedDealtobeupdated.intermedianryAgency = args.input.intermedianryAgency;
    missedDealtobeupdated.missionStart = new Date(args.input.missionStart);
    missedDealtobeupdated.missionEnd = new Date(args.input.missionEnd);
    missedDealtobeupdated.skills = skills;
    missedDealtobeupdated.jobTitle = args.input.jobTitle;
    missedDealtobeupdated.dailyRate = args.input.dailyRate;
    (missedDealtobeupdated.city = args.input.city),
      (missedDealtobeupdated.missionProjectDescription =
        args.input.missionProjectDescription);
    missedDealtobeupdated.candidate = candidate;
    missedDealtobeupdated.save();
    console.log(" missedDealtobeupdated", missedDealtobeupdated);

    console.log("missedDeal updated successfully by a CDO !");
    return {
      ...missedDealtobeupdated._doc,
      _id: missedDealtobeupdated.id,
      missionStart: new Date(
        missedDealtobeupdated._doc.missionStart
      ).toISOString(),
      missionEnd: new Date(missedDealtobeupdated._doc.missionEnd).toISOString(),
      creator: User.findById(missedDealtobeupdated._doc.creator)
    };
  } catch (error) {
    console.log("err", error);
    throw new Error(error);
  }
};

const findMissedDealsByColumn = async (args) => {
  try {
    let tab = [];
    const exist = (deal) => {
      let i = 0;
      for (i = 0; i < missedDeals.length; i += 1) {
        if (deal._id.toString() === missedDeals[i]._id.toString()) {
          tab.push(missedDeals[i]);
        }
      }
    };
    let missedDeals = [];
    // await args.searched.map(async(search) =>
    for (let i = 0; i < args.searched.length; i++) {
      if ("company" in args.searched[i]) {
        if (missedDeals.length < 1) {
          missedDeals = await MissedDeal.find({
            companyName: { $regex: args.searched[i].company, $options: "i" }
          });
        } else {
          let missedDeals1 = await MissedDeal.find({
            companyName: { $regex: args.searched[i].company, $options: "i" }
          });
          missedDeals1.map((deal) => {
            exist(deal);
          });
          missedDeals = [];
          tab.map((deal) => {
            missedDeals.push(deal);
          });
          tab = [];
        }
      } else if ("ITConsultingFirm" in args.searched[i]) {
        if (missedDeals.length < 1) {
          missedDeals = await MissedDeal.find({
            ITConsultingFirm: {
              $regex: args.searched[i].ITConsultingFirm,
              $options: "i"
            }
          });
        } else {
          let missedDeals2 = await MissedDeal.find({
            ITConsultingFirm: {
              $regex: args.searched[i].ITConsultingFirm,
              $options: "i"
            }
          });
          missedDeals2.map((deal) => {
            exist(deal);
          });
          missedDeals = [];
          tab.map((deal) => {
            missedDeals.push(deal);
          });
          tab = [];
        }
      } else if ("intermedianryAgency" in args.searched[i]) {
        if (missedDeals.length < 1) {
          missedDeals = await MissedDeal.find({
            intermedianryAgency: {
              $regex: args.searched[i].intermedianryAgency,
              $options: "i"
            }
          });
        } else {
          let missedDeals3 = await MissedDeal.find({
            intermedianryAgency: {
              $regex: args.searched[i].intermedianryAgency,
              $options: "i"
            }
          });
          missedDeals3.map((deal) => {
            exist(deal);
          });
          missedDeals = [];
          tab.map((deal) => {
            missedDeals.push(deal);
          });
          tab = [];
        }
      } else if ("missionStart" in args.searched[i]) {
        const splitSearched = args.searched[i].missionStart.split("T");
        const allMissedDeal = await MissedDeal.find();
        /* allMissedDeal.map(deal => {
                           let splitDate = deal.missionStart.toISOString().split("T");
                           if (splitDate[0] === splitSearched [0])
                           {
                             missedDeals.push(deal);
                           }
                   });*/
        if (missedDeals.length < 1) {
          allMissedDeal.map((deal) => {
            let splitDate = deal.missionStart.toISOString().split("T");
            if (splitDate[0] === splitSearched[0]) {
              missedDeals.push(deal);
            }
          });
        } else {
          let missedDeals4 = [];
          allMissedDeal.map((deal) => {
            let splitDate = deal.missionStart.toISOString().split("T");
            if (splitDate[0] === splitSearched[0]) {
              missedDeals4.push(deal);
            }
          });
          missedDeals4.map((deal) => {
            exist(deal);
          });
          missedDeals = [];
          tab.map((deal) => {
            missedDeals.push(deal);
          });
          tab = [];
        }
      } else if ("missionEnd" in args.searched[i]) {
        const splitSearched = args.searched[i].missionEnd.split("T");
        const allMissedDeal = await MissedDeal.find();
        /*allMissedDeal.map(deal => {
                      let splitDate = deal.missionEnd.toISOString().split("T");
                      if (splitDate[0] === splitSearched [0])
                      {
                        missedDeals.push(deal);
                      }
              });*/
        if (missedDeals.length < 1) {
          allMissedDeal.map((deal) => {
            let splitDate = deal.missionEnd.toISOString().split("T");
            if (splitDate[0] === splitSearched[0]) {
              missedDeals.push(deal);
            }
          });
        } else {
          let missedDeals5 = [];
          allMissedDeal.map((deal) => {
            let splitDate = deal.missionEnd.toISOString().split("T");
            if (splitDate[0] === splitSearched[0]) {
              missedDeals5.push(deal);
            }
          });
          missedDeals5.map((deal) => {
            exist(deal);
          });
          missedDeals = [];
          tab.map((deal) => {
            missedDeals.push(deal);
          });
          tab = [];
        }
      } else if ("createdAt" in args.searched[i]) {
        const splitSearched = args.searched[i].createdAt.split("T");
        const allMissedDeal = await MissedDeal.find();
        /*allMissedDeal.map(deal => {
                      let splitDate = deal.createdAt.toISOString().split("T");
                      if (splitDate[0] === splitSearched [0])
                      {
                        missedDeals.push(deal);
                      }
              });*/
        if (missedDeals.length < 1) {
          allMissedDeal.map((deal) => {
            let splitDate = deal.createdAt.toISOString().split("T");
            if (splitDate[0] === splitSearched[0]) {
              missedDeals.push(deal);
            }
          });
        } else {
          let missedDeals6 = [];
          allMissedDeal.map((deal) => {
            let splitDate = deal.createdAt.toISOString().split("T");
            if (splitDate[0] === splitSearched[0]) {
              missedDeals6.push(deal);
            }
          });
          missedDeals6.map((deal) => {
            exist(deal);
          });
          missedDeals = [];
          tab.map((deal) => {
            missedDeals.push(deal);
          });
          tab = [];
        }
      } else if ("jobTitle" in args.searched[i]) {
        if (missedDeals.length < 1) {
          missedDeals = await MissedDeal.find({
            jobTitle: { $regex: args.searched[i].jobTitle, $options: "i" }
          });
        } else {
          let missedDeals7 = await MissedDeal.find({
            jobTitle: { $regex: args.searched[i].jobTitle, $options: "i" }
          });
          missedDeals7.map((deal) => {
            exist(deal);
          });
          missedDeals = [];
          tab.map((deal) => {
            missedDeals.push(deal);
          });
          tab = [];
        }
      } else if ("skills" in args.searched[i]) {
        const countSimilarities = (a, b) => {
          let matches = 0;
          for (let i = 0; i < a.length; i++) {
            for (let j = 0; j < b.length; j++) {
              if (
                b[j].name.toLowerCase().search(a[i].name.toLowerCase()) !== -1
              ) {
                matches++;
              }
            }
          }
          return matches;
        };

        const compare = (a, b) => {
          const A = a.similarities;
          const B = b.similarities;
          let comparison = 0;
          if (A > B) comparison = 1;
          else if (A < B) comparison = -1;
          return comparison * -1;
        };

        try {
          // const splitSearched = args.searched[i].skills.toLowerCase().split(" ");
          const splitSearched = args.searched[i].skills;
          const allMissedDeal = await MissedDeal.find();
          let tab2 = [];
          //allMissedDeal.map(async(missedDeal) => {
          for (let i = 0; i < allMissedDeal.length; i++) {
            //const splitSkills = missedDeal.skills.toLowerCase().split(" ");
            /* const splitSkills = await Skill.find({
                      _id: { $in: allMissedDeal[i].skills.map(({ _id }) => _id) },
                    });*/
            const splitSkills = allMissedDeal[i].skills;

            let similarities = countSimilarities(splitSearched, splitSkills);
            if (similarities !== 0)
              tab2.push({
                missedDeal: allMissedDeal[i],
                similarities: similarities
              });
          }
          tab2 = tab2.sort(compare);
          if (missedDeals.length < 1) {
            tab2.map((deal) => {
              missedDeals.push(deal.missedDeal);
            });
          } else {
            let missedDeals8 = [];
            tab2.map((deal) => {
              missedDeals8.push(deal.missedDeal);
            });
            missedDeals8.map((deal) => {
              exist(deal);
            });
            missedDeals = [];
            tab.map((deal) => {
              missedDeals.push(deal);
            });
            tab = [];
          }
        } catch (error) {
          throw new Error(error);
        }
      } else if ("dailyRate" in args.searched[i]) {
        //missedDeals = await MissedDeal.find({ dailyRate: { $regex: args.searched, $options:'i' } });
        if (missedDeals.length < 1) {
          missedDeals = await MissedDeal.find({
            dailyRate: { $regex: args.searched[i].dailyRate, $options: "i" }
          });
        } else {
          let missedDeals9 = await MissedDeal.find({
            dailyRate: { $regex: args.searched[i].dailyRate, $options: "i" }
          });
          missedDeals9.map((deal) => {
            exist(deal);
          });
          missedDeals = [];
          tab.map((deal) => {
            missedDeals.push(deal);
          });
          tab = [];
        }
      } else if ("city" in args.searched[i]) {
        //missedDeals = await MissedDeal.find({ city: { $regex: args.searched, $options:'i' } });
        if (missedDeals.length < 1) {
          missedDeals = await MissedDeal.find({
            city: { $regex: args.searched[i].city, $options: "i" }
          });
        } else {
          let missedDeals10 = await MissedDeal.find({
            city: { $regex: args.searched[i].city, $options: "i" }
          });
          missedDeals10.map((deal) => {
            exist(deal);
          });
          missedDeals = [];
          tab.map((deal) => {
            missedDeals.push(deal);
          });
          tab = [];
        }
      } else if ("missionProjectDescription" in args.searched[i]) {
        //missedDeals = await MissedDeal.find({ missionProjectDescription: { $regex: args.searched, $options:'i' }});
        if (missedDeals.length < 1) {
          missedDeals = await MissedDeal.find({
            missionProjectDescription: {
              $regex: args.searched[i].missionProjectDescription,
              $options: "i"
            }
          });
        } else {
          let missedDeals11 = await MissedDeal.find({
            missionProjectDescription: {
              $regex: args.searched[i].missionProjectDescription,
              $options: "i"
            }
          });
          missedDeals11.map((deal) => {
            exist(deal);
          });
          missedDeals = [];
          tab.map((deal) => {
            missedDeals.push(deal);
          });
          tab = [];
        }
      } else if ("creator" in args.searched[i]) {
        const sourceur = await User.findOne({
          email: args.searched[i].creator
        });
        console.log("sourceur", sourceur);
        if (sourceur) {
          //missedDeals = await MissedDeal.find({ creator: new mongoose.Types.ObjectId(sourceur._id) });
          if (missedDeals.length < 1) {
            missedDeals = await MissedDeal.find({
              creator: new mongoose.Types.ObjectId(sourceur._id)
            });
          } else {
            let missedDeals12 = await MissedDeal.find({
              creator: new mongoose.Types.ObjectId(sourceur._id)
            });
            missedDeals12.map((deal) => {
              exist(deal);
            });
            missedDeals = [];
            tab.map((deal) => {
              missedDeals.push(deal);
            });
            tab = [];
          }
        } else {
          throw new Error("Sourceur do not exist!");
        }
      } else if ("candidate" in args.searched[i]) {
        const candidate = args.searched[i].candidate;
        if (candidate) {
          //missedDeals = await MissedDeal.find({ candidat: new mongoose.Types.ObjectId(candidat._id) });
          if (missedDeals.length < 1) {
            missedDeals = await MissedDeal.find({ candidate: candidate });
          } else {
            let missedDeals13 = await MissedDeal.find({ candidate: candidate });
            missedDeals13.map((deal) => {
              exist(deal);
            });
            missedDeals = [];
            tab.map((deal) => {
              missedDeals.push(deal);
            });
            tab = [];
          }
        } else {
          throw new Error("Candidate do not exist!");
        }
      }
    }
    if (missedDeals) {
      return missedDeals.map((missedDeal) => {
        return {
          ...missedDeal._doc,
          _id: missedDeal.id,
          missionStart: new Date(missedDeal._doc.missionStart).toISOString(),
          missionEnd: new Date(missedDeal._doc.missionEnd).toISOString(),
          createdAt: new Date(missedDeal._doc.createdAt).toISOString(),
          creator: User.findById(missedDeal._doc.creator)
          //candidate: Candidate.findById(missedDeal._doc.candidate)
        };
      });
    } else {
      throw new Error("missed deal do not exist!");
    }
  } catch (error) {
    throw new Error(error);
  }
};

const emailSending = async () => {
  try {
    const missedDeals = await MissedDeal.find();
    const dataManagers = await DataManager.find();
    const marketingManagers = await MarketingManager.find();
    // exporting mongodb data to csv
    if (missedDeals) {
      const csvWriter = createCsvWriter({
        path: "missedDeal.csv",
        header: [
          { id: "company", title: "company" },
          { id: "ITConsultingFirm", title: "ITConsultingFirm" },
          { id: "intermedianryAgency", title: "intermedianryAgency" },
          { id: "missionStart", title: "missionStart" },
          { id: "missionEnd", title: "missionEnd" },
          { id: "skills", title: "skills" },
          { id: "jobTitle", title: "jobTitle" },
          { id: "dailyRate", title: "dailyRate" },
          { id: "city", title: "city" },
          {
            id: "missionProjectDescription",
            title: "missionProjectDescription"
          }
        ]
      });

      csvWriter
        .writeRecords(missedDeals)
        .then(() => console.log("Write to misseDeal.csv successfully!"));
      let buff;
      await fs.readFile("./missedDeal.csv", (erreur, data) => {
        if (erreur) throw erreur;
        buff = data.toString("base64");

        dataManagers.map((manager) => {
          const request = mailjet
            .connect(
              process.env.MJ_APIKEY_PUBLIC,
              process.env.MJ_APIKEY_PRIVATE
            )
            .post("send", { version: "v3.1" })
            .request({
              Messages: [
                {
                  From: {
                    Email: "no-reply@club-freelance.com"
                  },
                  To: [
                    {
                      // "Email": manager.email,
                      Email: manager.email
                    }
                  ],
                  Subject: "Missed Deals.",
                  TextPart: "Missed deals list: \n",

                  Attachments: [
                    {
                      ContentType: "text/plain",
                      Filename: "missedDeal.csv",
                      Base64Content: buff
                    }
                  ]
                }
              ]
            });
          request
            .then((result) => {
              console.log(result.body);
            })
            .catch((err) => {
              console.log(err.statusCode);
            });
        });

        marketingManagers.map((manager) => {
          const request = mailjet
            .connect(
              process.env.MJ_APIKEY_PUBLIC,
              process.env.MJ_APIKEY_PRIVATE
            )
            .post("send", { version: "v3.1" })
            .request({
              Messages: [
                {
                  From: {
                    Email: "no-reply@club-freelance.com"
                  },
                  To: [
                    {
                      // "Email": manager.email,
                      Email: manager.email
                    }
                  ],
                  Subject: "Missed Deals.",
                  TextPart: "Missed deals list: \n",

                  Attachments: [
                    {
                      ContentType: "text/plain",
                      Filename: "missedDeal.csv",
                      Base64Content: buff
                    }
                  ]
                }
              ]
            });
          request
            .then((result) => {
              console.log(result.body);
            })
            .catch((err) => {
              console.log(err.statusCode);
            });
        });
      });
      //email sending to all data managers
    }
    const message = "email has been sent";
    return message;
    //email sending to all marketing managers
    /* marketingManagers.map(manager => {
              const request = mailjet
              .connect('0ba9eeebbbe2595fede5d98b2dfb3946', '925701fb3b61d5f1b3e7676539891c86')
              .post("send", {'version': 'v3.1'})
              .request({
              "Messages":[
                 {
                   "From": {
                             "Email": "safa.benturkia@esprit.tn",
                              "Name": "safa"
                           },
                   "To": [
                           {
                             "Email": manager.email,

                           }
                         ],
                  "Subject": "Greetings from Mailjet.",
                  "TextPart": "My first Mailjet email",
                  "HTMLPart": "<h3>Dear passenger 1, welcome to <a href='https://www.mailjet.com/'>Mailjet</a>!</h3><br />May the delivery force be with you!",
                  "CustomID": "AppGettingStartedTest"
                 }
                ]
              })
             request
             .then((result) => {
             console.log(result.body)
              })
             .catch((err) => {
                console.log(err.statusCode)
             })
           });*/
  } catch (error) {
    throw new Error(error);
  }
};

const missedDealHelpers = {
  get_missedDeals,
  get_missedDeal,
  createMissedDeal,
  updateMissedDeal,
  emailSending,
  findMissedDealsByColumn
};

module.exports = missedDealHelpers;
