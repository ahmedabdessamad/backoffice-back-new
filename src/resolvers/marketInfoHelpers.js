const mongoose = require("mongoose");
const models = require("../models");
const {
  User,
  BusinessSector,
  Sales,
  MarketInfo,
  ClientCorporation,
  Category
} = models;
const mailjet = require("node-mailjet");
const { bhEntitySvc } = require("../helpers/bullhorn/bhSession");

const get_marketInfos = async () => {
  try {
    let marketInfos = await MarketInfo.find();
    marketInfos = marketInfos.reverse();
    return marketInfos.map((marketInfo) => {
      return {
        ...marketInfo._doc,
        _id: marketInfo.id,
        createdAt: new Date(marketInfo._doc.createdAt).toISOString(),
        creator: User.findById(marketInfo._doc.creator)
      };
    });
  } catch (error) {
    throw new Error(error);
  }
};
const get_marketInfo = async (args) => {
  try {
    const marketInfo = await MarketInfo.findById(args.marketInfoID);
    if (marketInfo) {
      return {
        ...marketInfo._doc,
        _id: marketInfo.id,
        createdAt: new Date(marketInfo._doc.createdAt).toISOString(),
        creator: User.findById(marketInfo._doc.creator)
      };
    } else {
      throw new Error("Market do not exist!");
    }
  } catch (error) {
    throw new Error(error);
  }
};

const findMarketInfosByColumn = async (args) => {
  try {
    let tab = [];
    const exist = (market) => {
      let i = 0;
      for (i = 0; i < marketInfos.length; i += 1) {
        if (market._id.toString() === marketInfos[i]._id.toString()) {
          tab.push(marketInfos[i]);
        }
      }
    };
    let marketInfos = [];
    // await args.searched.map(async(search) =>
    for (let i = 0; i < args.searched.length; i++) {
      if ("company" in args.searched[i]) {
        if (marketInfos.length < 1) {
          marketInfos = await MarketInfo.find({
            companyName: { $regex: args.searched[i].company, $options: "i" }
          });
        } else {
          let marketInfos1 = await MarketInfo.find({
            companyName: { $regex: args.searched[i].company, $options: "i" }
          });
          marketInfos1.map((market) => {
            exist(market);
          });
          marketInfos = [];
          tab.map((market) => {
            marketInfos.push(market);
          });
          tab = [];
        }
      } else if ("jobTitle" in args.searched[i]) {
        if (marketInfos.length < 1) {
          marketInfos = await MarketInfo.find({
            jobTitle: { $regex: args.searched[i].jobTitle, $options: "i" }
          });
        } else {
          let marketInfos2 = await MarketInfo.find({
            jobTitle: { $regex: args.searched[i].jobTitle, $options: "i" }
          });

          marketInfos2.map((market) => {
            exist(market);
          });
          marketInfos = [];
          tab.map((market) => {
            marketInfos.push(market);
          });
          tab = [];
        }
      } else if ("contact" in args.searched[i]) {
        if (marketInfos.length < 1) {
          marketInfos = await MarketInfo.find({
            contact: { $regex: args.searched[i].contact, $options: "i" }
          });
        } else {
          let marketInfos3 = await MarketInfo.find({
            contact: { $regex: args.searched[i].contact, $options: "i" }
          });

          marketInfos3.map((market) => {
            exist(market);
          });
          marketInfos = [];
          tab.map((market) => {
            marketInfos.push(market);
          });
          tab = [];
        }
      } else if ("contactName" in args.searched[i]) {
        if (marketInfos.length < 1) {
          marketInfos = await MarketInfo.find({
            contactName: { $regex: args.searched[i].contactName, $options: "i" }
          });
        } else {
          let marketInfos3 = await MarketInfo.find({
            contactName: { $regex: args.searched[i].contactName, $options: "i" }
          });

          marketInfos3.map((market) => {
            exist(market);
          });
          marketInfos = [];
          tab.map((market) => {
            marketInfos.push(market);
          });
          tab = [];
        }
      } else if ("email" in args.searched[i]) {
        if (marketInfos.length < 1) {
          marketInfos = await MarketInfo.find({
            email: { $regex: args.searched[i].email, $options: "i" }
          });
        } else {
          let marketInfos4 = await MarketInfo.find({
            email: { $regex: args.searched[i].email, $options: "i" }
          });

          marketInfos4.map((market) => {
            exist(market);
          });
          marketInfos = [];
          tab.map((market) => {
            marketInfos.push(market);
          });
          tab = [];
        }
      } else if ("phone" in args.searched[i]) {
        if (marketInfos.length < 1) {
          marketInfos = await MarketInfo.find({
            phone: { $regex: args.searched[i].phone, $options: "i" }
          });
        } else {
          let marketInfos5 = await MarketInfo.find({
            phone: { $regex: args.searched[i].phone, $options: "i" }
          });

          marketInfos5.map((market) => {
            exist(market);
          });
          marketInfos = [];
          tab.map((market) => {
            marketInfos.push(market);
          });
          tab = [];
        }
      } else if ("industry" in args.searched[i]) {
        const countSimilarities = (a, b) => {
          let matches = 0;
          for (let i = 0; i < a.length; i++) {
            for (let j = 0; j < b.length; j++) {
              if (
                b[j].name.toLowerCase().search(a[i].name.toLowerCase()) !== -1
              ) {
                matches++;
              }
            }
          }
          return matches;
        };

        const compare = (a, b) => {
          const A = a.similarities;
          const B = b.similarities;
          let comparison = 0;
          if (A > B) comparison = 1;
          else if (A < B) comparison = -1;
          return comparison * -1;
        };

        try {
          // const splitSearched = args.searched[i].skills.toLowerCase().split(" ");
          const splitSearched = args.searched[i].industry;
          const allMarketInfo = await MarketInfo.find();
          let tab2 = [];
          for (let i = 0; i < allMarketInfo.length; i++) {
            const splitIndustry = await BusinessSector.find({
              _id: { $in: allMarketInfo[i].industry.map(({ _id }) => _id) }
            });
            let similarities = countSimilarities(splitSearched, splitIndustry);
            if (similarities !== 0)
              tab2.push({
                marketInfo: allMarketInfo[i],
                similarities: similarities
              });
          }

          tab2 = tab2.sort(compare);
          if (marketInfos.length < 1) {
            tab2.map((market) => {
              marketInfos.push(market.marketInfo);
            });
          } else {
            let marketInfos8 = [];
            tab2.map((market) => {
              marketInfos8.push(market.marketInfo);
            });

            marketInfos8.map((market) => {
              exist(market);
            });
            marketInfos = [];
            tab.map((market) => {
              marketInfos.push(market);
            });
            tab = [];
          }
        } catch (error) {
          throw new Error(error);
        }
      } else if ("description" in args.searched[i]) {
        if (marketInfos.length < 1) {
          marketInfos = await MarketInfo.find({
            description: { $regex: args.searched[i].description, $options: "i" }
          });
        } else {
          let marketInfos7 = await MarketInfo.find({
            description: { $regex: args.searched[i].description, $options: "i" }
          });

          marketInfos7.map((market) => {
            exist(market);
          });
          marketInfos = [];
          tab.map((market) => {
            marketInfos.push(market);
          });
          tab = [];
        }
      } else if ("createdAt" in args.searched[i]) {
        const splitSearched = args.searched[i].createdAt.split("T");
        const allMarketInfo = await MarketInfo.find();

        if (marketInfos.length < 1) {
          allMarketInfo.map((market) => {
            let splitDate = market.createdAt.toISOString().split("T");
            if (splitDate[0] === splitSearched[0]) {
              marketInfos.push(market);
            }
          });
        } else {
          let marketInfos8 = [];
          allMarketInfo.map((market) => {
            let splitDate = market.createdAt.toISOString().split("T");
            if (splitDate[0] === splitSearched[0]) {
              marketInfos8.push(market);
            }
          });
          marketInfos8.map((market) => {
            exist(market);
          });
          marketInfos = [];
          tab.map((market) => {
            marketInfos.push(market);
          });
          tab = [];
        }
      } else if ("categories" in args.searched[i]) {
        const countSimilarities = (a, b) => {
          let matches = 0;
          for (let i = 0; i < a.length; i++) {
            for (let j = 0; j < b.length; j++) {
              if (
                b[j].name.toLowerCase().search(a[i].name.toLowerCase()) !== -1
              ) {
                matches++;
              }
            }
          }
          return matches;
        };

        const compare = (a, b) => {
          const A = a.similarities;
          const B = b.similarities;
          let comparison = 0;
          if (A > B) comparison = 1;
          else if (A < B) comparison = -1;
          return comparison * -1;
        };

        try {
          const splitSearched = args.searched[i].categories;
          const allMarketInfo = await MarketInfo.find();
          let tab2 = [];
          for (let i = 0; i < allMarketInfo.length; i++) {
            const splitCategories = await Category.find({
              _id: { $in: allMarketInfo[i].categories.map(({ _id }) => _id) }
            });
            let similarities = countSimilarities(
              splitSearched,
              splitCategories
            );
            if (similarities !== 0)
              tab2.push({
                marketInfo: allMarketInfo[i],
                similarities: similarities
              });
          }

          tab2 = tab2.sort(compare);
          if (marketInfos.length < 1) {
            tab2.map((market) => {
              marketInfos.push(market.marketInfo);
            });
          } else {
            let marketInfos8 = [];
            tab2.map((market) => {
              marketInfos8.push(market.marketInfo);
            });

            marketInfos8.map((market) => {
              exist(market);
            });
            marketInfos = [];
            tab.map((market) => {
              marketInfos.push(market);
            });
            tab = [];
          }
        } catch (error) {
          throw new Error(error);
        }
      } else if ("creator" in args.searched[i]) {
        const sourceur = await User.findOne({
          email: args.searched[i].creator
        });
        if (sourceur) {
          if (marketInfos.length < 1) {
            marketInfos = await MarketInfo.find({
              creator: new mongoose.Types.ObjectId(sourceur._id)
            });
          } else {
            let marketInfos10 = await MarketInfo.find({
              creator: new mongoose.Types.ObjectId(sourceur._id)
            });
            marketInfos10.map((market) => {
              exist(market);
            });
            marketInfos = [];
            tab.map((market) => {
              marketInfos.push(market);
            });
            tab = [];
          }
        } else {
          throw new Error("Sourceur do not exist!");
        }
      }
    }
    if (marketInfos) {
      return marketInfos.map((marketInfo) => {
        return {
          ...marketInfo._doc,
          _id: marketInfo.id,
          createdAt: new Date(marketInfo._doc.createdAt).toISOString(),
          creator: User.findById(marketInfo._doc.creator)
        };
      });
    } else {
      throw new Error("market info do not exist!");
    }
  } catch (error) {
    throw new Error(error);
  }
};

const getCandidateEmailPhone = async (contact, bhEntitySvc) => {
  return await bhEntitySvc.findEntityAsync("Candidate", contact, [
    "email",
    "mobile"
  ]);
};
const candidateEmailPHoneBH = async (args) => {
  try {
    const ss2 = await getCandidateEmailPhone(
      args.candidateID,
      await bhEntitySvc
    );
    console.log("candidateName", ss2);
    return ss2.data;
  } catch (error) {
    throw new Error(error);
  }
};
const getCandidateName = async (contact, bhEntitySvc) => {
  return await bhEntitySvc.findEntityAsync("Candidate", contact, ["name"]);
};
const createMarketInfo = async (args, user) => {
  try {
    // Sales list
    const sales = await Sales.find();
    const creator = await User.findOne({
      _id: new mongoose.Types.ObjectId(user._id)
    }).lean();

    const businessSector = await BusinessSector.find({
      name: { $in: args.input.industry.map(({ name }) => name) }
    });
    const categories = await Category.find({
      name: { $in: args.input.categories.map(({ name }) => name) }
    });

    let comp1 = {};
    let compId = "0";
    const company = await ClientCorporation.findOne({
      name: args.input.company
    }).lean();
    if (company) {
      comp1 = company.name;
      compId = company.id;
    } else {
      comp1 = args.input.company;
    }

    const ss2 = await getCandidateName(args.input.contact, await bhEntitySvc);
    console.log("candidateName", ss2);

    const marketInfo = new MarketInfo({
      companyId: compId,
      companyName: comp1,
      jobTitle: args.input.jobTitle,
      contact: args.input.contact,
      contactName: ss2.data.name,
      email: args.input.email,
      phone: args.input.phone,
      industry: businessSector,
      categories: categories,
      description: args.input.description,
      creator: creator
    });

    marketInfo.save();
    //add market Info to the sourceur
    /*creator.createdMarketInfo.push(marketInfo);
    creator.save();*/
    //add market Info to the company
    /* company.MissedDeals.push(missedDeal);
                 company.save();*/
    console.log("marketInfo.categories", categories);
    //email sending to all Sales
    sales.map((sale) => {
      const request = mailjet
        .connect(process.env.MJ_APIKEY_PUBLIC, process.env.MJ_APIKEY_PRIVATE)
        .post("send", { version: "v3.1" })
        .request({
          Messages: [
            {
              From: {
                Email: "no-reply@club-freelance.com"
              },
              To: [
                {
                  Email: sale.email
                }
              ],
              Subject: "New market info!",
              TextPart: "",
              HTMLPart: `<h3>New Market Info added </h3><br /> 
              Company: ${marketInfo.companyName} <br />
              Job title:  ${marketInfo.jobTitle} <br />
              Email: ${marketInfo.email} <br />
              Phone:  ${marketInfo.phone} <br />
              Industry: ${marketInfo.industry.map((indus) => indus.name)} <br />
              Skills: ${categories.map((cat) => cat.name)} <br />
              Description: ${marketInfo.description} <br /> <br />
              Best regards.`
            }
          ]
        });
      request
        .then((result) => {
          console.log(result.body);
        })
        .catch((err) => {
          console.log(err.statusCode);
        });
    });
    const request = mailjet
      .connect(process.env.MJ_APIKEY_PUBLIC, process.env.MJ_APIKEY_PRIVATE)
      .post("send", { version: "v3.1" })
      .request({
        Messages: [
          {
            From: {
              Email: "no-reply@club-freelance.com"
            },
            To: [
              {
                Email: "flemaignent@club-freelance.com"
              }
            ],
            Subject: "A market info has just been added ",
            TextPart: "",
            // eslint-disable-next-line no-irregular-whitespace
            HTMLPart: `The market info is about the Company : ${
              marketInfo.companyName
            }
            </br>
            </br>
            </br>

            Industry of the company: ${marketInfo.industry.map(
              (indus) => indus.name
            )}
            </br>
            </br>
             
            It has been provided by the candidate ID : ${
              marketInfo.contact
            } </br>
            Name:  ${marketInfo.contactName} </br>  </br>
             
            He/She is working has a :  ${marketInfo.jobTitle} </br>
            Email :  ${marketInfo.email} </br>
            Phone: ${marketInfo.phone} </br>
            Community:  ${categories.map((cat) => cat.name)} </br>  </br>
             
            Description of the info : </br> 
            ${marketInfo.description}  `,
            CustomID: "AppGettingStartedTest"
          }
        ]
      });
    request
      .then((result) => {
        console.log(result.body);
      })
      .catch((err) => {
        console.log(err.statusCode);
      });

    console.log("market Info added successfully ");
    // return marketInfo;
    return {
      ...marketInfo._doc,
      _id: marketInfo.id,
      creator: User.findById(marketInfo._doc.creator)
    };
  } catch (error) {
    throw new Error(error);
  }
};

const updateMarketInfo = async (args) => {
  try {
    let marketInfotobeupdated = null;

    marketInfotobeupdated = await MarketInfo.findById(args.marketInfoID);

    // BusinessSector
    const businessSector = await BusinessSector.find({
      name: { $in: args.input.industry.map(({ name }) => name) }
    });
    const categories = await Category.find({
      name: { $in: args.input.categories.map(({ name }) => name) }
    });

    const company = await ClientCorporation.findOne({
      name: args.input.company
    }).lean();
    let comp1 = {};
    let compId = "0";

    if (company) {
      comp1 = company.name;
      compId = company.id;
    } else {
      comp1 = args.input.company;
    }

    marketInfotobeupdated.companyId = compId;
    marketInfotobeupdated.companyName = comp1;
    marketInfotobeupdated.jobTitle = args.input.jobTitle;
    marketInfotobeupdated.contact = args.input.contact;
    marketInfotobeupdated.email = args.input.email;
    marketInfotobeupdated.phone = args.input.phone;
    marketInfotobeupdated.industry = businessSector.map(({ _id }) => _id);
    marketInfotobeupdated.categories = categories.map(({ _id }) => _id);
    marketInfotobeupdated.description = args.input.description;

    marketInfotobeupdated.save();

    console.log("missedDeal updated successfully!");

    return {
      ...marketInfotobeupdated._doc,
      _id: marketInfotobeupdated.id,
      creator: User.findById(marketInfotobeupdated._doc.creator)
    };
  } catch (error) {
    console.log("err", error);
    throw new Error(error);
  }
};

const marketInfoHelpers = {
  get_marketInfos,
  get_marketInfo,
  createMarketInfo,
  updateMarketInfo,
  findMarketInfosByColumn,
  candidateEmailPHoneBH
};

module.exports = marketInfoHelpers;
