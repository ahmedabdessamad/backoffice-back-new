const models = require("../models");
const CDO = models.CDO;

const CDOResolvers = {
  Query: {},
  Mutation: {
    addCDO: async (parent, { email, userID }) => {
      try {
        //check conditions
        if (!email || !userID) {
          throw new Error("invalid CDO");
        }
        const newCDO = {
          email: email,
          userID: userID
        };
        //create CDO
        const CDO1 = await CDO.create(newCDO);
        return {
          ...CDO1.toObject()
        };
      } catch (error) {
        throw new Error(error);
      }
    }
  },
  CDO: {
    CDOID: (parent) => parent._id
  }
};

module.exports = CDOResolvers;
