import {
  getExcludedEmails,
  getMailingServices,
  getSentEmails
} from "../../ms/mailer";
import { requestTimesheet } from "../../ms/timesheet";
const { checkPermissionAndGetRole } = require("../../helpers/authorization");
const isAuthenticated = require("../../helpers/isAuthenticated");
import ApprovalQueries from "../timesheet-manager/approvals/queries";

const { approvals } = ApprovalQueries;

const queries = {
  excludedEmails: async (parent, args) => {
    const { filter } = args;

    const res = await getExcludedEmails(filter);
    return {
      excludes: res.excludes,
      total: res.total
    };
  },
  getServices: async () => await getMailingServices(),
  sentEmails: async (parent, args, ctx) => {
    const { filter } = args;
    let userTimesheetIDs = [];
    const user = isAuthenticated(ctx);
    const UserRole = checkPermissionAndGetRole({ TIMESHEETEMAILS: 6 }, user);

    if (UserRole === "Editor") {
      //get connected user approvals
      const userApprovals = await approvals(
        null,
        {
          filter: { ownerIDs: [user.bhID] }
        },
        {}
      );

      //get user's placementIDs
      if (userApprovals) {
        const userPlacementIDs = userApprovals.map((ap) => ap.placementID);
        console.log("userPlacementIDs", userPlacementIDs);
        //get user's timesheets by userPlacementIDs
        const filterTimesheet = { placementIDs: userPlacementIDs };
        const { timesheets } = await requestTimesheet(
          "get",
          "/timesheets",
          null,
          filterTimesheet
        );
        console.log("userTimesheets", timesheets);
        // get user's TimesheetIDs
        userTimesheetIDs = timesheets.map((ts) => ts.id);
        console.log("userTimesheetIDs", userTimesheetIDs);
      }
    }
    // get sent emails
    const res = await getSentEmails(filter);
    let sents = [];
    console.log("type of", typeof userTimesheetIDs[0]);
    if (userTimesheetIDs.length > 0) {
      sents = res.excludes.filter((sent) =>
        userTimesheetIDs.includes(Number.parseInt(sent.param, 10))
      );
    } else sents = res.excludes;

    return {
      sent: sents,
      total: res.total
    };
  }
};

export default queries;
