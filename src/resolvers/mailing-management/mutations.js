import { excludeEmail, resumeEmail, sendEmailService } from "../../ms/mailer";
import isValidEmail from "../../helpers/isValidEmail";
import {requestTimesheet} from "../../ms/timesheet";
const mutations = {
  stopMailing: async (parent, args) => {
    const { email, services } = args;
    if (!isValidEmail(email)) throw new Error("INVALID INPUT");
    await excludeEmail(email, services);
    return "OK";
  },
  resumeMailing: async (parent, args) => {
    const { email } = args;
    if (!isValidEmail(email)) throw new Error("INVALID INPUT");
    await resumeEmail(email);
    return "OK";
  },
  sendTimesheetEmail: async (parent, args) => {
    const { service, action, placementId, month, year, timesheetId } = args;

     let timesheetIdToSend = null;
    // resend coming from logs
    if(placementId && month && year){
      const  filter  = {placementIDs:[placementId],month: month, year: year };
      // get timesheet id by placementId month and year
      const { timesheets } = await requestTimesheet(
        "get",
        "/timesheets",
        null,
        filter
      );
      timesheetIdToSend = timesheets[0].id;
    } else if (timesheetId) timesheetIdToSend = timesheetId;   // resend coming from emails

    // send Email
    try {

      await sendEmailService("timesheets", action, timesheetIdToSend);
    } catch (e) {
      console.log("Error sending email: ", e);
    }
    return "OK";
  }
};

export default mutations;
