import mutations from "./mutations";
import queries from "./queries";

const mailingManagement = {
  Query: queries,
  Mutation: mutations
};

module.exports = mailingManagement;
