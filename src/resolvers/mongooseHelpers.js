const { UserInputError } = require("apollo-server-express");
const {
  notifyUserOnSignUp,
  notifyUserOnResetPassword
} = require("../helpers/emailFns");
const models = require("../models");

const {
  randomPassword,
  encryptPassword,
  comparedPassword,
  getToken
} = require("../helpers/auth");

const { User, CorporateUser, Permission } = models;

const getUsers = async () => {
  try {
    return await User.find().sort({ _id: -1 }).populate("permissions").lean();
  } catch (err) {
    console.error(err);
    throw err;
  }
};

const getUser = async (args) => {
  try {
    return await User.findOne({ _id: args.userID }).lean();
  } catch (e) {
    console.error(e);
    throw e;
  }
};

const insert = async (_, args) => {
  let checkUser = null;

  checkUser = await User.findOne({ email: args.input.email }).lean();
  if (checkUser) throw new UserInputError("user already exists");

  const newPassword = randomPassword();
  console.log(" args.input.permissions", args.input.permissions);

  const permissions = await Permission.find({
    _id: { $in: args.input.permissions }
  });
  console.log("permissions", permissions);
  const newUser = {
    username: args.input.username,
    password: await encryptPassword(newPassword),
    email: args.input.email,
    permissions: permissions
  };
  // match user with BH user
  try {
    const corporateUser = await CorporateUser.findOne({
      email: args.input.email
    }).lean();
    if (corporateUser) newUser.bhID = corporateUser.id;
  } catch (e) {
    console.log("Error BH", e);
  }

  // create user
  let user = await User.create(newUser);

  // user = await user.populate("permission.groupe").execPopulate();

  try {
    notifyUserOnSignUp(user, newPassword);
  } catch (e) {
    console.log("Error sending email", e);
  }

  // creating a token from user payload
  const userToHash = { ...user };
  delete userToHash.password;
  delete userToHash.__v;
  const token = getToken(userToHash);
  console.log("user registred successfully:  ");

  return {
    ...user.toObject(),
    token
  };
};

const update = async (_, args) => {
  const {
    input: { password }
  } = args;
  console.log("passwordpassword", password);
  const permissions = await Permission.find({
    _id: { $in: args.input.permissions }
  }).lean();
  let updateObj = {};
  if (password) {
    updateObj = {
      username: args.input.username,
      password: await encryptPassword(password),
      email: args.input.email,
      permissions: permissions
    };
  } else {
    updateObj = {
      username: args.input.username,
      email: args.input.email,
      permissions: permissions
    };
  }

  try {
    const user = await User.findOne({ _id: args.userID });
    if (password) {
      const checkPassword = await comparedPassword(password, user.password);
      console.log("checkPassword", checkPassword);
      if (!checkPassword) {
        notifyUserOnResetPassword(user, null, "update", password);
      }
    }
  } catch (e) {
    throw e;
  }
  await User.findByIdAndUpdate(args.userID, updateObj);
  return User.findById(args.userID).populate("permissions").lean();
};

const deleteById = async (userID) => {
  await User.findByIdAndRemove(userID);
  return "OK";
};
const mongooseHelpers = { getUsers, getUser, insert, update, deleteById };

module.exports = mongooseHelpers;
