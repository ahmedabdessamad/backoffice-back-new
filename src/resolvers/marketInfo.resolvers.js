const {
  checkPermission,
  checkSpecificPermission
} = require("../helpers/authorization");
const isAuthenticated = require("../helpers/isAuthenticated");
const models = require("../models");
const { Category, BusinessSector, MarketInfo } = models;

const {
  get_marketInfos,
  get_marketInfo,
  createMarketInfo,
  updateMarketInfo,
  findMarketInfosByColumn,
  candidateEmailPHoneBH
} = require("./marketInfoHelpers");

const marketInfoResolvers = {
  Query: {
    marketInfos: async (parent, args, context) => {
      const user = isAuthenticated(context);
      checkPermission({ MARKETINFO: 7 }, user);
      return await get_marketInfos();
    },
    marketInfo: async (parent, args, context) => {
      const user = isAuthenticated(context);
      checkPermission({ MARKETINFO: 7 }, user);
      return await get_marketInfo(args);
    },

    findMarket: async (parent, args, context) => {
      const user = isAuthenticated(context);
      checkPermission({ MARKETINFO: 7 }, user);
      return await findMarketInfosByColumn(args);
    },

    candidateEmailPHone: async (parent, args, context) => {
      const user = isAuthenticated(context);
      console.log("user", user);
      return await candidateEmailPHoneBH(args);
    }
  },
  Mutation: {
    addMarketInfo: async (parent, args, context) => {
      const user = isAuthenticated(context);
      checkPermission({ MARKETINFO: 6 }, user);
      return await createMarketInfo(args, user);
    },
    editMarketInfo: async (parent, args, context) => {
      try {
        const user = isAuthenticated(context);
        const marketInfotobeupdated = await MarketInfo.findById(
          args.marketInfoID
        );
        checkSpecificPermission(
          { MARKETINFO: 6 },
          user,
          marketInfotobeupdated.creator
        );
        return await updateMarketInfo(args);
      } catch (error) {
        console.log("err", error);
        throw new Error(error);
      }
    }
  },
  MarketInfo: {
    marketInfoID: (parent) => {
      return parent._id;
    },
    industry: async (parent) => {
      return BusinessSector.find({ _id: { $in: parent.industry } });
    },
    categories: async (parent) => {
      return Category.find({ _id: { $in: parent.categories } });
    }
  },
  BusinessSector: {
    id: (parent) => {
      return parent._id;
    }
  },
  User: {
    userID: (parent) => {
      return parent._id;
    }
  },
  Category: {
    id: (parent) => {
      return parent._id;
    }
  }
};

module.exports = marketInfoResolvers;
