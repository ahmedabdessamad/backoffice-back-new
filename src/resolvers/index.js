import { sync } from "glob";
import { resolve } from "path";

const resolvers = sync("./**/*.resolvers.js").map((file) =>
  require(resolve(file))
);

export default resolvers;
