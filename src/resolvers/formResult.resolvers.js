const checkPermission = require("../helpers/authorization/checkPermission");
const isAuthenticated = require("../helpers/isAuthenticated");
const {
  get_formResults,
  get_formResult,
  createFormResult,
  get_Results_of_one_form
} = require("./formResultHelpers");

const formResultResolvers = {
  Query: {
    formResults: async (parent, args, context) => {
     // const user = isAuthenticated(context);
     // checkPermission({ FORM: 4 }, user);
      return await get_formResults();
    },
    formResult: async (parent, args, context) => {
     // const user = isAuthenticated(context);
     // checkPermission({ FORMRESULTS: 4, EDITFORMRESULT: 4 }, user);
      return await get_formResult(args);
    },

    ResultOfOneForm: async (parent, args, context) => {
     // const user = isAuthenticated(context);
     // checkPermission({ FORMRESULTS: 4, EDITFORMRESULT: 4 }, user);
      return await get_Results_of_one_form(args);
    }
  },
  Mutation: {
    // addFormResult: createFormResult
    addFormResult: async (parent, args) => {
      return await createFormResult(args);
    }
  },
  FormResult: {
    formResultID: (parent) => {
      return parent._id;
    }
  }
};

module.exports = formResultResolvers;
