const models = require("../models");
const BusinessSector = models.BusinessSector;

const BusinessSectorResolvers = {
  Query: {
    businessSectors: async () => {
      return await BusinessSector.find().lean();
    },
    businessSector: async (parent, args) => {
      return await BusinessSector.findOne({ _id: args.id }).lean();
    }
  },
  Mutation: {
    addBusinessSector: async (parent, args) => {
      const { name } = args;
      return await BusinessSector.create({
        name,
        dateAdded: 55555555555
      });
    }
  },
  BusinessSector: {
    id: (parent) => parent._id
  }
};

module.exports = BusinessSectorResolvers;
