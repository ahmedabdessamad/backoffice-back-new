import { ObjectId } from "mongoose";
import { verify } from "jsonwebtoken";
import runtimeVars from "../configs/runtimeVars";

const models = require("../models");

const { SECRET_WORD } = runtimeVars;
const { FormResult, User, Form } = models;

const get_formResults = async () => {
  try {
    let formResults = await FormResult.find();
    formResults = formResults.reverse();
    return formResults.map((formResult) => {
      return {
        ...formResult._doc,
        _id: formResult.id,
        createdAt: new Date(formResult._doc.createdAt).toISOString()
      };
    });
  } catch (error) {
    throw new Error(error);
  }
};
const get_Results_of_one_form = async (args) => {
  try {
    let formResults = await FormResult.find({
      formID: args.formID
    });
    formResults = formResults.reverse();
    return formResults.map((formResult) => {
      return {
        ...formResult._doc,
        _id: formResult.id,
        createdAt: new Date(formResult._doc.createdAt).toISOString()
      };
    });
  } catch (error) {
    throw new Error(error);
  }
};
const get_formResult = async (args) => {
  try {
    const formResult = await FormResult.findById(args.formResultID);
    if (formResult) {
      return {
        ...formResult._doc,
        _id: formResult.id,
        createdAt: new Date(formResult._doc.createdAt).toISOString()
      };
    } else {
      throw new Error("FormResult do not exist!");
    }
  } catch (error) {
    throw new Error(error);
  }
};
const createFormResult = async (args) => {
  try {
    // connected user
    /* const { user } = context;
    const creator = await User.findOne({
      _id: new mongoose.Types.ObjectId(user._id)
    });*/
    console.log("heree", args)
    const clientEamil = verify(args.clientID, SECRET_WORD);
    console.log("heree22",clientEamil)
    const form22 = verify(args.formID, SECRET_WORD);
   console.log( "hereeee33", form22 );
    const obj = JSON.parse(JSON.stringify(args.answers));
    const form = await Form.findById(form22);
    const creator = await User.findOne({
      _id: form.creator
    });
    // creator = await User.find({ creator: new mongoose.Types.ObjectId(sourceur._id) });
    // add FormResult
    const formResult = new FormResult({
      formID: form22,
      formTitle: form.subject,
      formCreator: creator.username,
      clientID: clientEamil,
      answers: obj
    });

    formResult.save();
    //add missed deal to the sourceur
    //creator.createdMissedDeals.push(missedDeal);
    //creator.save();

    return formResult;
  } catch (error) {
    throw new Error(error);
  }
};

const formResultHelpers = {
  get_formResults,
  get_formResult,
  createFormResult,
  get_Results_of_one_form
};

module.exports = formResultHelpers;
