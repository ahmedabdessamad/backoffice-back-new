const models = require("../models");
const MarketingManager = models.MarketingManager;

const MarketingManagerResolvers = {
  Query: {},
  Mutation: {
    /// ajout d'un marketing manager
    addMarketingManager: async (parent, { email, userID }) => {
      try {
        //check conditions
        if (!email || !userID) {
          throw new Error("invalid Marketing Manager");
        }
        const newMarketingManager = {
          email: email,
          userID: userID
        };
        //create Marketing Manager
        const marketingManager = await MarketingManager.create(
          newMarketingManager
        );
        console.log("marketing manager registred successfully ");
        return {
          ...marketingManager.toObject()
        };
      } catch (error) {
        throw new Error(error);
      }
    }
  },
  MarketingManager: {
    marketingManagerID: (parent) => {
      return parent._id;
    }
  }
};

module.exports = MarketingManagerResolvers;
