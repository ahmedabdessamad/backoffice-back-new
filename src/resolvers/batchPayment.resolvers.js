const path = require("path");
const fs = require("fs");

const generateSepa = require("../helpers/xeroHelpers/generateSepa");
const hsbcConnect = require("../helpers/hsbcConnect");
const initiateXero = require("../helpers/xeroHelpers/initiateXero");
const findDocument = require("../helpers/findDocument");
const getInvoice = require("../helpers/xeroHelpers/getInvoice");
const BatchPayment = require("../models/batchPayment");
const { Hsbc, Cron, tokenSet } = require("../models");

/** */
const saveFile = (data, fName) => {
  const filePath = `./files/xml/${fName}`;
  const dirPath = path.dirname(filePath);
  try {
    if (!fs.existsSync(dirPath)) {
      fs.mkdirSync(dirPath, { recursive: true });
    }
  } catch (e) {
    throw new Error(`ERROR_CREATING_DIR: ${e}`);
  }
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, data, function (err) {
      if (err) return reject(err);
      console.log("The file was saved!");
      return resolve(filePath);
    });
  });
};

const batchPaymentResolvers = {
  Query: {
    batchPayments: async (parent, args) => {
      try {
        return await BatchPayment.find({
          validationStatus: { $in: args.validationStatus }
        }).lean();
      } catch (error) {
        throw Error(error);
      }
    },
    batchPayment: async (parent, args) => {
      const { batchPaymentID } = args;

      return await BatchPayment.findOne({
        batchPaymentID
      }).lean();
    },
    getLastUpdate: async () => {
      const lastExecution = await Cron.findOne({});
      // console.log(lastExecution);
      return lastExecution.updatedAt;
    },
    getBatchPaymentByFilter: async (parent, args) => {
      const {
        batchPaymentID,
        status,
        type,
        validationStatus,
        organisation
      } = args.filter;

      const actualFilter = {};
      if (batchPaymentID) {
        actualFilter.batchPaymentID = { $in: batchPaymentID };
      }
      if (status) {
        actualFilter.status = { $in: status };
      }
      if (type) {
        actualFilter.type = { $in: type };
      }
      if (validationStatus) {
        actualFilter.validationStatus = { $in: validationStatus };
      }
      if (organisation) {
        actualFilter.organisation = { $in: organisation };
      }

      // console.log("filter:", actualFilter);
      try {
        return await BatchPayment.find(actualFilter).lean();
      } catch (e) {
        console.error(e);
      }
    },
    getBatchPaymentsPerPage: async (parent, args) => {
      const { page, limit } = args;
      try {
        return await BatchPayment.find()
          .limit(limit)
          .skip((page - 1) * limit)
          .lean();
      } catch (e) {
        console.error(e);
      }
    },
    verifData: async () => {
      return await findDocument(tokenSet);
    },
    getExpiredToken: async () => {
      const cron = await Cron.findOne();
      return cron && cron.error;
    }
  },
  Mutation: {
    generateSepa: async (parent, args) => {
      const fileName = `${Date.now()}-sepa.xml`;
      let requestedSepa;
      try {
        requestedSepa = await BatchPayment.findOne({
          batchPaymentID: args.batchPaymentID,
          status: { $ne: "DELETED" }
        }).lean();
      } catch (e) {
        // console.log("error generating xml =", e);
        throw Error("Error Generating XML", e);
      }

      if (!requestedSepa || !requestedSepa.payments)
        throw new Error("NO BATCHPAYMENT EXISTS");

      try {
        let invoices = [];
        const leng = requestedSepa.payments.length;
        for (let i = 0; i < leng; i = i + 5) {
          const chunk = requestedSepa.payments.slice(i, i + 5);

          invoices = invoices.concat(
            await Promise.all(
              chunk.map((p) =>
                getInvoice(p.invoice.invoiceID, requestedSepa.tenant)
              )
            )
          );
        }

        requestedSepa.payments = requestedSepa.payments.map((p) => {
          const invoice = invoices.filter(
            (inv) => inv.invoiceID === p.invoice.invoiceID
          )[0];

          return { ...p, invoice };
        });
      } catch (e) {
        console.log("error getting invoice from xero", e);
      }
      const xmldoc = generateSepa(requestedSepa);

      await saveFile(xmldoc, fileName);
      try {
        await BatchPayment.findOneAndUpdate(
          { batchPaymentID: args.batchPaymentID },
          {
            validationStatus: "EXPORTED"
          }
        );
      } catch (e) {
        console.log("Error exporting batchPayment");
      }

      return `${process.env.HOST}/files/xml/${fileName}`;
    },

    validateBatchPayment: async (parent, args) => {
      try {
        const requestedHsbc = await Hsbc.findOne({
          batchPaymentID: args.batchPaymentID
        });

        const response = await hsbcConnect(JSON.stringify(requestedHsbc.data));
        console.log(response);
        return response._links.consentApproval.href;
      } catch (e) {
        throw new Error(e.message);
      }
    },
    discardBatchPayment: async (parent, args) => {
      try {
        return await BatchPayment.findOneAndUpdate(
          { batchPaymentID: args.batchPaymentID },
          {
            $set: {
              validationStatus: "REJECTED"
            }
          },
          { new: true }
        );
      } catch (e) {
        throw new Error("Error discarding batch payment");
      }
    },
    buildConsentUrl: async () => {
      const xero = await initiateXero();

      try {

        const consentUrl = await xero.buildConsentUrl();
        console.log(consentUrl);
        return consentUrl;
      } catch (err) {
        throw new Error("Sorry, something went wrong");
      }
    }
  },
  Link: {
    url: (parent) => {
      return parent;
    }
  }
};
module.exports = batchPaymentResolvers;
