const {
  checkPermission,
  checkSpecificPermission
} = require("../helpers/authorization");
const isAuthenticated = require("../helpers/isAuthenticated");
const models = require("../models");
const { MissedDeal } = models;
const {
  get_missedDeals,
  get_missedDeal,
  createMissedDeal,
  updateMissedDeal,
  findMissedDealsByColumn
} = require("./missedDealHelpers");

const missedDealResolvers = {
  Query: {
    missedDeals: async (parent, args, context) => {
      const user = isAuthenticated(context);
      checkPermission({ MISSEDDEAL: 7 }, user);
      return await get_missedDeals();
    },
    missedDeal: async (parent, args, context) => {
      const user = isAuthenticated(context);
      checkPermission({ MISSEDDEAL: 7 }, user);
      return await get_missedDeal(args);
    },

    find: async (parent, args, context) => {
      const user = isAuthenticated(context);
      checkPermission({ MISSEDDEAL: 7 }, user);
      return await findMissedDealsByColumn(args);
    }
  },
  Mutation: {
    addMissedDeal: async (parent, args, context) => {
      const user = isAuthenticated(context);
      checkPermission({ MISSEDDEAL: 6 }, user);
      return await createMissedDeal(args, user);
    },

    editMissedDeal: async (parent, args, context) => {
      try {
        const user = isAuthenticated(context);
        const missedDealtobeupdated = await MissedDeal.findById(
          args.missedDealID
        );
        checkSpecificPermission(
          { MISSEDDEAL: 6 },
          user,
          missedDealtobeupdated.creator
        );
        return await updateMissedDeal(args, user);
      } catch (error) {
        console.log("err", error);
        throw new Error(error);
      }
    }
  },
  MissedDeal: {
    missedDealID: (parent) => {
      return parent._id;
    }
  },
  Candidate: {
    candidateID: (parent) => {
      return parent._id;
    }
  },
  User: {
    userID: (parent) => {
      return parent._id;
    }
  },
  ClientCorporation: {
    id: (parent) => {
      return parent._id;
    }
  }
};

module.exports = missedDealResolvers;
