const models = require("../models");
const Permission = models.Permission;

const permissionResolvers = {
  Query: {
    permissions: async () => {
      return await Permission.find().sort({ createdAt: -1 }).lean();
    },
    permission: async (parent, args) => {
      const permission = await Permission.findOne({ _id: args.id });
      return { ...permission.toObject() };
    }
  },
  Mutation: {
    addPermission: async (parent, args) => {
      const newPermission = {
        name: args.input.name,
        tabs: args.input.tabs
      };
      const permission = await Permission.create(newPermission);
      return { ...permission.toObject() };
    },
    editPermission: async (parent, args) => {
      await Permission.updateOne(
        { _id: args.id },
        {
          $set: {
            name: args.input.name,
            tabs: args.input.tabs
          }
        },
        { new: true }
      );
      return "permission updated";
    },
    deletePermission: async (parent, args) => {
      await Permission.findByIdAndRemove({ _id: args.id });
      return "permission deleted";
    }
  },
  Permission: {
    id: (parent) => parent._id
  }
};

module.exports = permissionResolvers;
