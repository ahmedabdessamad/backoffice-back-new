import { getLogs } from "../../ms/logs";
const { checkPermissionAndGetRole } = require("../../helpers/authorization");
const isAuthenticated = require("../../helpers/isAuthenticated");
import ApprovalQueries from "../timesheet-manager/approvals/queries";

const { approvals } = ApprovalQueries;

const queries = {
  getLogs: async (parent, args, ctx) => {
    const user = isAuthenticated(ctx);
    const UserRole = checkPermissionAndGetRole(
      { TIMESHEETLOGS: 6, LOGS: 6 },
      user
    );
    //console.log("UserRole", user.bhID);
    const { filter } = args;
    if (UserRole === "Editor") {
      // filter.ownerIDs = [user.bhID];
      const userApprovals = await approvals(
        null,
        {
          filter: { ownerIDs: [user.bhID] }
        },
        {}
      );
      //console.log("userApprovals", userApprovals);
      if (!userApprovals.length) filter.placementIDs = 0;
      else {
        let userPlacementIDs = userApprovals.map(
          ({ placementID }) => placementID
        );
        if ("placementIDs" in filter) {
          filter.placementIDs = userPlacementIDs.filter((id) =>
            filter.placementIDs.includes(id)
          );
          if (!filter.placementIDs.length) filter.placementIDs = 0;
        } else {
          filter.placementIDs = userPlacementIDs;
        }
      }
    }
   // console.log("filter : ", filter);
    const res = await getLogs(filter);
    return {
      logs: res.logs,
      total: res.total
    };
  }
};

export default queries;
