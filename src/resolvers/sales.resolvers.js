const models = require("../models");
const Sales = models.Sales;

const salesResolvers = {
  Query: {},
  Mutation: {
    addSales: async (parent, { email, userID }) => {
      try {
        //check conditions
        if (!email || !userID) {
          throw new Error("invalid sale");
        }
        const newSales = {
          email: email,
          userID: userID
        };
        //create sale
        const sales = await Sales.create(newSales);
        console.log("sale registred successfully ");
        return {
          ...sales.toObject()
        };
      } catch (error) {
        throw new Error(error);
      }
    }
  },
  Sales: {
    salesID: (parent) => parent._id
  }
};

module.exports = salesResolvers;
