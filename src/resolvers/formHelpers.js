import { verify } from "jsonwebtoken";

const mongoose = require("mongoose");
const models = require("../models");
const mailjet = require("node-mailjet");
const jwt = require("jsonwebtoken");
import runtimeVars from "../configs/runtimeVars";

const { SECRET_WORD, FORMS_HOST } = runtimeVars;

const { Form, User } = models;
const { getToken } = require("../helpers/auth");

const get_forms = async () => {
  try {
    let forms = await Form.find();
    forms = forms.reverse();
    return forms.map((form) => {
      return {
        ...form._doc,
        _id: form.id,
        createdAt: new Date(form._doc.createdAt).toISOString(),
        creator: User.findById(form._doc.creator)
      };
    });
  } catch (error) {
    throw new Error(error);
  }
};

const get_form = async (args) => {
  try {
    //console.log("args111111111111", args);
    const form = await Form.findById(args.formID);
    //console.log("form",form);
    if (form) {
      //console.log("form", form);
      return {
        ...form._doc,
        _id: form.id,
        createdAt: new Date(form._doc.createdAt).toISOString(),
        creator: User.findById(form._doc.creator)
      };
    } else {
      throw new Error("Form do not exist!");
    }
  } catch (error) {
    throw new Error(error);
  }
};
const get_form2 = async (args) => {
  try {
    //console.log("args form2", args);
    const form2 = jwt.verify(args.formID, SECRET_WORD);
    const form = await Form.findById(form2);
    if (form) {
      //console.log("form", form);
      return {
        ...form._doc,
        _id: form.id,
        createdAt: new Date(form._doc.createdAt).toISOString(),
        creator: User.findById(form._doc.creator)
      };
    } else {
      throw new Error("Form do not exist!");
    }
  } catch (error) {
    throw new Error(error);
  }
};
const createForm = async (args, user) => {
  const creator = await User.findOne({
    _id: new mongoose.Types.ObjectId(user._id)
  });

  /* for(let i = 0 ; i < args.structure.length; i++){
        tab.push(JSON.parse(JSON.stringify(args.structure[i])));
        console.log("tabbbbb",typeof(JSON.parse(JSON.stringify(args.structure[i]))));
    }*/
  //console.log("args====>",JSON.parse(JSON.stringify(args.structure[0])));
  //console.log("tab", tab);

  const obj = JSON.parse(JSON.stringify(args.structure));

  // add Form
  //const form = Form.create({subject:"Form22", structure:obj, creator: creator});
  const form = new Form({
    subject: args.subject,
    structure: obj,
    creator: creator
  });

  form.save();
  //add missed deal to the sourceur
  //creator.createdMissedDeals.push(missedDeal);
  //creator.save();

  return form;
};

const updateForm = async (args) => {
  try {
    let formtobeupdated = null;

    const obj = JSON.parse(JSON.stringify(args.structure));
    formtobeupdated = await Form.findById(args.formID);

    // update missedDeal

    formtobeupdated.subject = args.subject;
    formtobeupdated.structure = obj;

    formtobeupdated.save();
    // console.log(" formtobeupdated", formtobeupdated);
    return {
      ...formtobeupdated._doc,
      _id: formtobeupdated.id,
      creator: User.findById(formtobeupdated._doc.creator)
    };
  } catch (error) {
    throw new Error(error);
  }
};

const formSending = async (args) => {
  try {
    let formtobeupdated = null;
    const emailsSplit = args.email.split(";");
    formtobeupdated = await Form.findById(args.formID);
    const token0 = getToken(args.formID);

    emailsSplit.map((email) => {
      const token = getToken(email);

      const request = mailjet
        .connect(process.env.MJ_APIKEY_PUBLIC, process.env.MJ_APIKEY_PRIVATE)
        .post("send", { version: "v3.1" })
        .request({
          Messages: [
            {
              From: {
                Email: "no-reply@club-freelance.com"
              },
              To: [
                {
                  Email: email
                }
              ],
              Subject: "Form",
              TextPart: "",
              HTMLPart: `<h3>Please complete this form!</h3><br /> <a href="${FORMS_HOST}/Form/${token0}?email=${token}">Form link</a><br /> Best regards.`,
              CustomID: "AppGettingStartedTest"
            }
          ]
        });
      request
        .then((result) => {
          console.log(result.body);
        })
        .catch((err) => {
          console.error(err.statusCode);
        });
    });

    // return true;
    if (formtobeupdated) {
      // console.log("formmmmmm",formtobeupdated);
      return {
        ...formtobeupdated._doc,
        _id: formtobeupdated.id,
        creator: User.findById(formtobeupdated._doc.creator)
      };
    }
  } catch (error) {
    throw new Error(error);
  }
};

const formHelpers = {
  get_forms,
  get_form,
  get_form2,
  createForm,
  updateForm,
  formSending
};

module.exports = formHelpers;
