import { sign } from "jsonwebtoken";
import runtimeVars from "../configs/runtimeVars";
import { AuthenticationError } from "apollo-server-express";

const { getToken, comparedPassword } = require("../helpers/auth");
const checkPermission = require("../helpers/authorization/checkPermission");
const isAuthenticated = require("../helpers/isAuthenticated");
const { notifyUserOnResetPassword } = require("../helpers/emailFns");
const { encryptPassword } = require("../helpers/auth");
const models = require("../models");
const { User, TokenReset } = models;

const { CLIENT_HOST } = runtimeVars;
const {
  getUsers,
  getUser,
  update,
  deleteById,
  insert
} = require("./mongooseHelpers");

const userResolvers = {
  Query: {
    me: async (parent, args, context) => {
      // Checking For LoggedIn Status Obtained from 'context'.
      if (context.loggedIn) {
        // Resolving the user payload received from 'context'.
       // console.log("userrrrrrrrrrrrrrrrrrr", context.user);
        //update the user state after changing his permision
        return await User.findOne({ _id: context.user._id })
          .populate("permissions")
          .lean();
      } else {
        // Error on Not loggedIn status.
        throw new AuthenticationError("Please Login .");
      }
    },
    // users: checkPermission([{ role: "Admin", groupe: "SuperAdmin" }], getUsers),
    users: async (parent, args, context) => {
      const user = isAuthenticated(context);
      checkPermission({ USERS: 4 }, user);
      return getUsers();
    },
    user: async (parent, args, context) => {
      const user = isAuthenticated(context);
      checkPermission({ USERS: 4 }, user);
      return await getUser(args, user);
    },
    greeting: async (parent, args, context) => {
      const user = isAuthenticated(context);
      checkPermission({ USERS: 3 }, user);
      return "Hello";
    }
  },
  Mutation: {
    login: async (parent, args) => {
      try {
        const user = await User.findOne({ email: args.email })
          .populate("permissions")
          .lean();

        if (!user) throw new Error("no user found");
        const isMatch = await comparedPassword(args.password, user.password);

        if (isMatch) {
          // protect password
          delete user.password;
          delete user.__v;
          const token = getToken(user);
          return { ...user, token };
        } else {
          throw new Error("wrong password!");
        }
      } catch (error) {
        throw new AuthenticationError(error.message);
      }
    },

    requestReset: async (parent, args) => {
      const user = await User.findOne({ email: args.email }).lean();
      if (!user) throw new Error("no user found with that email");

      const { password, createdAt, _id } = user;
      const secret = password + "-" + createdAt;
      const resetToken = sign({ _id }, secret);
      const resetTokenExpiry = Date.now() + 3600000; //1h
      await TokenReset.findOneAndUpdate(
        {
          user: _id
        },
        {
          reset_password_token: resetToken,
          reset_password_expires: resetTokenExpiry
        },
        {
          upsert: true
        }
      );
      // sending the email
      try {
        notifyUserOnResetPassword(
          user,
          `${CLIENT_HOST}/password/reset/${user._id}/${resetToken}`,
          "login",
          null
        );
      } catch (e) {
        console.error("Error sending email", e);
      }
      return true;
    },
    changePassword: async (parent, args) => {
      const { email, password, confirmPassword } = args;

      if (password !== confirmPassword) {
        throw new Error(`unmatched passwords`);
      }
      return await User.findOneAndUpdate(
        { email },
        {
          $set: {
            password: await encryptPassword(password)
          }
        },
        { new: true }
      ).lean();
    },
    resetPassword: async (parent, args) => {
      const { userId, password, confirmPassword, resetToken } = args;

      if (password !== confirmPassword) {
        throw new Error(`unmatched passwords`);
      }

      // find the user with that resetToken
      // make sure it's not expired

      const user = await TokenReset.findOne({
        reset_password_token: resetToken,
        reset_password_expires: { $gte: Date.now() }
      });
      if (!user) {
        throw new Error(
          "Your password reset token is either invalid or expired."
        );
      }
      // update user with the new password in the db

      return await User.findOneAndUpdate(
        { _id: userId },
        {
          $set: {
            password: await encryptPassword(password)
          }
        },
        { new: true }
      ).lean();
    },
    //addUser: checkPermission([{ role: "Admin", groupe: "SuperAdmin" }], insert),
    addUser: insert,
    editUser: update,
    deleteUser: async (parent, args) => {
      const { userID } = args;
      return deleteById(userID);
    }
    /* deleteUser: async (parent, args, context) => {
      const user = isAuthenticated(context);
      checkPermission({ USERS: 4 }, user);
      return await deleteById(args);
    }*/
  },
  User: {
    userID: (parent) => {
      // console.log(userResolvers);
      return parent._id;
    }
  },
  Permission: {
    id: (parent) => parent._id,
    name: (parent) => parent.name
    /*id: (parent) => {
      return parent.map(({ _id }) => _id).toString();
    },
    name: (parent) => {
      return parent.map(({ name }) => name).toString();
    }*/
  }
};

module.exports = userResolvers;
