import { requestTimesheet } from "../../../ms/timesheet";

const holidaysResolvers = {
  Query: {
    holidays: async (parent, args) => {
      const { filter } = args;
      const { holidays } = await requestTimesheet(
        "get",
        "/holidays",
        null,
        filter
      );
      return holidays;
    }
  }
};
module.exports = holidaysResolvers;
