import queries from "./queries";
import mutations from "./mutations";
import resolvers from "./resolvers";

const approval = {
  Query: queries,
  Mutation: mutations,
  Approval: resolvers
};

module.exports = approval;
