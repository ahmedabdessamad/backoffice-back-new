import {
  getCandidateFromBH,
  getClientFromBH,
  getCorporateUserFromBH,
  getPlacementFromBH
} from "../../../helpers/bullhorn";

const resolvers = {
  total: async (parent, args, ctx) => ctx.total,
  client: async (parent) => ({ id: parent.clientID }), //getClientFromBH(parent.clientID),
  candidate: async (parent) => ({ id: parent.candidateID }), //getCandidateFromBH(parent.candidateID),
  approvingClient: async (parent) => ({ id: parent.approvingClientID }), //getClientFromBH(parent.approvingClientID),
  approvingClient2: async (parent) => ({ id: parent.approvingClientID2 }),
  // getClientFromBH(parent.approvingClientID2),
  title: async (parent) => {
    try {
      const { placementID } = parent;
      const pl = { id: placementID }; //await getPlacementFromBH(placementID);
      return pl.jobOrder.title;
    } catch (e) {
      return null;
    }
  },
  owner: async (parent) => ({ id: parent.ownerID }) //getCorporateUserFromBH(parent.ownerID)
};

export default resolvers;
