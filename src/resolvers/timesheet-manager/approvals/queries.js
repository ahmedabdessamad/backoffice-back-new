import { requestTimesheet } from "../../../ms/timesheet";

const queries = {
  approvals: async (parent, args, ctx) => {
    let { filter } = args;
    const { approvals, total } = await requestTimesheet(
      "get",
      `/approvals`,
      null,
      filter
    );
    ctx.total = total;
    return approvals;
  }
};

export default queries;
