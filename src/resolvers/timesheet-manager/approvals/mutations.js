import { requestTimesheet } from "../../../ms/timesheet";

const mutations = {
  updateApproval: async (parent, args) => {
    const { placementID, approvingClientID, approvingClientID2 } = args;
    if (!approvingClientID2 && !approvingClientID2) return "OK";
    const body = { placementID };
    if (approvingClientID) body.approvingClientID = approvingClientID;
    if (approvingClientID2) body.approvingClientID2 = approvingClientID2;

    await requestTimesheet("put", "/approvals", body);
    return "OK";
  },
  deleteApproval: async (parent, args) => {
    const { placementID, approvingClient, approvingClient2 } = args;
    const body = { placementID };
    if (approvingClient) body.approvingClientID = -1;
    if (approvingClient2) body.approvingClientID2 = -1;

    await requestTimesheet("put", "/approvals", body);
    return "OK";
  }
};

export default mutations;
