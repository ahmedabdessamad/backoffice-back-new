import { requestTimesheet } from "../../../ms/timesheet";

const queries = {
  reports: async (parent, args, ctx) => {
    const { filter } = args;
    const { reports, total } = await requestTimesheet(
      "get",
      "/reports",
      null,
      filter
    );
    ctx.total = total;
    return reports;
  }
};

export default queries;
