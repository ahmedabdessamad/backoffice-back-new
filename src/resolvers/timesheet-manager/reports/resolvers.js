import { reportFileLink } from "../../../ms/resources";
import { getClientFromBH } from "../../../helpers/bullhorn";

const resolvers = {
  total: (parent, args, ctx) => ctx.total,
  frFile: (parent) => {
    try {
      const { frFile } = parent;
      if (!frFile) return null;
      return reportFileLink(parent.timesheetID);
    } catch (e) {
      return "";
    }
  },
  enFile: (parent) => {
    try {
      const { enFile } = parent;
      if (!enFile) return null;
      return reportFileLink(parent.timesheetID);
    } catch (e) {
      return "";
    }
  },
  madeBy: (parent) => getClientFromBH(parent.madeBy),
  placementID: (parent) => parent.placementID
};

export default resolvers;
