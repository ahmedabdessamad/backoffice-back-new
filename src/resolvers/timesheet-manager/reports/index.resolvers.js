import queries from "./queries";
import resolvers from "./resolvers";

const report = {
  Query: queries,
  Report: resolvers
};

module.exports = report;
