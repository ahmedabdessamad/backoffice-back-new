import { requestTimesheet } from "../../../ms/timesheet";

const mutations = {
  updateTimesheet: async (parent, args) => {
    console.log("heeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeere", args);
    const { id, newTimesheet } = args;
    const response = await requestTimesheet(
      "put",
      `/timesheets/${id}`,
      newTimesheet
    );

    console.log("response ==>", response);
    if (response)
    return "OK";
    return "kO";
  },
  deleteTimesheet: async (parent, args) => {
    const { id } = args;
    await requestTimesheet("delete", `timesheets/${id}`);
    return "OK";
  }
};

export default mutations;
