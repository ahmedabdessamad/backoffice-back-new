import { conversationResolver } from "../helpers";
import { getCandidateFromBH, getClientFromBH } from "../../../helpers/bullhorn";

const resolvers = {
  total: (parent, args, ctx) => ctx.total,
  client: async (parent) => getClientFromBH(parent.clientID),
  candidate: async (parent) => getCandidateFromBH(parent.candidateID),
  conversation: async (parent) => conversationResolver(parent.id),
  statusMaker: async (parent) => {
    const { statusMaker } = parent;
    try {
      const user = await getClientFromBH(statusMaker, ["name", "id"]);
      if (user) return user;
    } catch (e) {}
    try {
      const user = await getCandidateFromBH(statusMaker, ["name", "id"]);
      if (user) return user;
    } catch (e) {}
    return null;
  }
};

export default resolvers;
