import queries from "./queries";
import mutations from "./mutations";
import resolvers from "./resolvers";

const timesheet = {
  Query: queries,
  Mutation: mutations,
  Timesheet: resolvers
};

module.exports = timesheet;
