import { requestTimesheet } from "../../../ms/timesheet";
const {
  checkPermissionAndGetRole,
  checkPermission
} = require("../../../helpers/authorization");
const isAuthenticated = require("../../../helpers/isAuthenticated");
import ApprovalQueries from "../approvals/queries";

const { approvals } = ApprovalQueries;

const queries = {
  timesheets: async (parent, args, ctx) => {
    const user = isAuthenticated(ctx);
    const UserRole = checkPermissionAndGetRole({ TIMESHEETS: 7 }, user);
    const { filter } = args;
    if (UserRole === "Editor" || UserRole === "Reader") {
      filter.ownerIDs = [user.bhID];
    }
    console.log("filter", filter);
    const { timesheets, total } = await requestTimesheet(
      "get",
      "/timesheets",
      null,
      filter
    );
    ctx.total = total;
    console.log("timesheets", timesheets);
    return timesheets;
    /* if (UserRole === "Admin") return timesheets;
    if (UserRole === "Editor") {
      try {
        const userApprovals = await approvals(
          null,
          {
            filter: { ownerID: user.bhID }
          },
          {}
        );

        const userPlacementIds = userApprovals.map(
          ({ placementID }) => placementID
        );

        const result = timesheets.filter(({ placementID }) =>
          userPlacementIds.includes(placementID)
        );
        ctx.total = result.length;
        return result;
      } catch (e) {
        console.log("Error line 35 = ", e);
        return [];
      }
    }*/
  },
  timesheet: async (parent, args, ctx) => {
    //const user = isAuthenticated(ctx);
    //checkPermission({ TIMESHEETS: 7 }, user);
    const { id, filter } = args;
    const { timesheet } = await requestTimesheet(
      "get",
      `/timesheets/${id}`,
      filter
    );

    return timesheet;
  }
};

export default queries;
