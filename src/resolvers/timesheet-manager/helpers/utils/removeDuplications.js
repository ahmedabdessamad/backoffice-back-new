const removeDuplication = (arr) => {
  const set = new Set(arr);
  return Array.from(set);
};

export default removeDuplication;
