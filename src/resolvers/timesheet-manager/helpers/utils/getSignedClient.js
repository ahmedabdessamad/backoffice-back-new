import { requestCore } from "../../../../ms/gateway-core";

const NOT_FOUND = new Error("NOT_SIGNED");

const getSignedClient = async (id) => {
  let client = await requestCore(
    `{clientById(id: ${id}){email firstname lastname}}`,
    {
      id
    }
  );
  client = client.data.data.clientById;
  if (!client || !client.firstname) throw NOT_FOUND;
  return {
    id,
    firstName: client.firstname,
    lastName: client.lastname,
    email: client.email,
    name: `${client.firstname} ${client.lastname}`
  };
};
export default getSignedClient;
