const onlyDuplicated = (arr1, arr2) => {
  if (!Array.isArray(arr1)) return [];
  if (!Array.isArray(arr2)) return arr1;
  return arr1.filter((i) => arr2.includes(i));
};

export default onlyDuplicated;
