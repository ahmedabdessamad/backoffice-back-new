const appendIDs = (filter, field, ids) => {
  if (!Array.isArray(ids)) return;
  if (Array.isArray(filter[field])) filter[field] = [...filter[field], ids];
  filter[field] = ids;
};

export default appendIDs;
