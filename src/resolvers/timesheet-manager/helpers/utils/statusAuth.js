import { UNAUTHORIZED } from "../../../../helpers/errors";

const statusAuth = (role, status) => {
  if (role === "CLIENT" && ["approved", "rejected"].includes(status)) return;
  if (role === "CANDIDATE" && ["pending"].includes(status)) return;
  throw UNAUTHORIZED;
};

export default statusAuth;
