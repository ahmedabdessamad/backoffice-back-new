const sum = (hours, minutes) => {
  const mn = Number.isInteger(minutes) ? minutes : 0;
  const h = Number.isInteger(hours) ? hours : 0;
  return h + mn / 60;
};

const workedTime = (timesheet) => {
  const total =
    timesheet.selectedDays.reduce((a, i) => a + sum(i.hours, i.minutes), 0) +
    timesheet.specialHours.reduce((a, i) => sum(i.hours, i.minutes), 0);
  return {
    days: Math.floor(total / 8),
    hours: Math.floor(total % 8),
    minutes: Math.round(((total % 8) % 1) * 60)
  };
};

export default workedTime;
