import { getCandidateFromBH } from "../../../../helpers/bullhorn";

const candidateResolver = async (candidateID) => {
  if (!candidateID) return null;
  const candidate = {
    id: candidateID,
    firstName: "",
    lasName: "",
    email: "",
    city: ""
  };
  try {
    const res = await getCandidateFromBH(candidateID);
    candidate.firstName = res.firstname;
    candidate.lastName = res.lastname;
    candidate.name = res.name || `${res.firstname} ${res.lastname}`;
    candidate.email = res.email;
    candidate.city =
      (res.address && (res.address.city || res.address.state)) || "";
    return candidate;
  } catch (e) {
    return candidate;
  }
};

export default candidateResolver;
