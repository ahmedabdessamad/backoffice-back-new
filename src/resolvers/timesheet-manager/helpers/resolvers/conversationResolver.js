import { requestTimesheet } from "../../../../ms/timesheet";

const conversationResolver = async (timesheetID) => {
  if (!timesheetID) return null;
  try {
    const { conversation } = await requestTimesheet(
      "get",
      `/conversations/${timesheetID}`
    );
    return conversation;
  } catch (e) {
    return null;
  }
};

export default conversationResolver;
