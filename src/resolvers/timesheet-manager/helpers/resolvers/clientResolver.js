import { getClientFromBH } from "../../../../helpers/bullhorn";

const clientResolver = async (clientID) => {
  if (!clientID) return null;

  const client = {
    id: clientID,
    firstName: "",
    lasName: "",
    email: "",
    companyName: ""
  };

  try {
    const res = await getClientFromBH(clientID);
    client.firstName = res.firstname;
    client.lastName = res.lastname;
    client.name = res.name || `${res.firstname} ${res.lastname}`;
    client.email = res.email;
    client.companyName = res.companyName;
    return client;
  } catch (e) {
    console.log(e);
    return client;
  }
};

export default clientResolver;
