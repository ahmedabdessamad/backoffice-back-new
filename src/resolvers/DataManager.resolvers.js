const models = require("../models");
const DataManager = models.DataManager;

const DataManagerResolvers = {
  Query: {},
  Mutation: {
    addDataManager: async (parent, { email, userID }) => {
      try {
        //check conditions
        if (!email || !userID) throw new Error("invalid Data Manager");

        const newDataManager = {
          email: email,
          userID: userID
        };

        //create Data Manager
        return await DataManager.create(newDataManager);
      } catch (error) {
        throw new Error(error);
      }
    }
  },
  DataManager: {
    dataManagerID: (parent) => parent._id
  }
};

module.exports = DataManagerResolvers;
