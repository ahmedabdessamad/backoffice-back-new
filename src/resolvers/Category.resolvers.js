const models = require("../models");
const Category = models.Category;

const CategoryResolvers = {
  Query: {
    categories: async () => {
      const categories = await Category.find({});
      categories.map((category) => {
        return {
          ...category.toObject()
        };
      });
      return categories;
    },
    skills: async () => {
      let skills = [];
      const result = await Category.find().select("skills");

      result.forEach((row) => {
        if (row.skills.total) skills = [...skills, ...row.skills.data];
      });
      return skills;
    },
    category: async (parent, args) => {
      const category = await Category.findOne({ _id: args.id });
      return { ...category.toObject() };
    }
  },
  Mutation: {},
  Category: {
    id: (parent) => {
      return parent._id;
    }
  }
};
//CategoryResolvers.Query.categories();
module.exports = CategoryResolvers;
