const models = require("../models");
const ClientCorporation = models.ClientCorporation;

const ClientCorporationResolvers = {
  Query: {
    clientCorporations: async () => {
      return await ClientCorporation.find().lean();
    }
  },
  Mutation: {},
  ClientCorporation: {
    id: (parent) => parent._id
  }
};

module.exports = ClientCorporationResolvers;
