import { verify } from "jsonwebtoken";
import runtimeVars from "../configs/runtimeVars";
const models = require("../models");
const { User } = models;
const { SECRET_WORD } = runtimeVars;

const isAuthenticated = async (cookie) => {
  if (!cookie || !cookie.AUTH_TOKEN) return { loggedIn: false };
  const token = cookie.AUTH_TOKEN;
  if (!token) return { loggedIn: false };
  try {
    const payload = verify(token, SECRET_WORD);

    const user = await User.findOne({ _id: payload._id })
      .populate("permissions")
      .lean();
    return { loggedIn: true, user: user };
  } catch (err) {
    return { loggedIn: false };
  }
};

export default isAuthenticated;
