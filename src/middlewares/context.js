import { verify } from "jsonwebtoken";
import runtimeVars from "../configs/runtimeVars";
const models = require("../models");
const { User } = models;
const { SECRET_WORD } = runtimeVars;

const context =async ({ req }) => {
  const token = req.headers.authorization;
  if (!token) return { loggedIn: false };
  try {
    const payload = verify(token.slice(7), SECRET_WORD);
    const user = await User.findOne({ _id: payload._id })
      .populate("permissions")
      .lean();
    return { loggedIn: true, user: user };
  } catch (err) {
    return { loggedIn: false };
  }
};

export default context;
