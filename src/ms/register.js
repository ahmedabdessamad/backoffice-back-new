import axios from "axios";
import { sign } from "jsonwebtoken";
import runtimeVars from "../configs/runtimeVars";
const {
  MS_NAME,
  HOST,
  SECRET_WORD,
  SERVER_REGISTRY_LINK,
  SERVER_REGISTRY_SECRET
} = runtimeVars;

const register = () => {
  const data = {
    name: MS_NAME,
    host: HOST,
    secret: SECRET_WORD
  };
  const payload = sign(data, SERVER_REGISTRY_SECRET);
  return axios
    .post(SERVER_REGISTRY_LINK, { payload })
    .then(() => console.info(`✅ ${MS_NAME} has been successfully registred`))
    .catch((e) => console.error(e));
};

export default register;
