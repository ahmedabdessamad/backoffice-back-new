import { msConfigs, request } from "../helpers";

const requestCore = async (query, variables) => {
  const { host, secret } = msConfigs("RESOURCES");
  return request("post", host, secret, {
    operationName: null,
    variables,
    query
  });
};

export default requestCore;
