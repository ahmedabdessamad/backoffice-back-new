const onFail = (fileData) => {
  console.log("fail on upload report", fileData.fileName);
};

export default onFail;
