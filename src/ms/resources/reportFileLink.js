import { msConfigs } from "../helpers";

const reportFileLink = (timesheetID) => {
  const { host } = msConfigs("GATEWAY_BACKOFFICE");
  return `${host}/report/${timesheetID}`;
};

export default reportFileLink;
