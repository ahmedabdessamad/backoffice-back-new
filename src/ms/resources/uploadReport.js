import onFail from "./onFail";
import { request, msConfigs } from "../helpers";

const uploadReport = async (fileBase64, fileName, timesheetID) => {
  const fileData = {
    timesheetID,
    fileName,
    fileType: "pdf",
    fileBase64
  };

  try {
    const { host, secret } = msConfigs("RESOURCES");
    const url = `${host}/json/reports`;
    const uploaded = await request("post", url, secret, fileData);
    return uploaded.data.result.report._id;
  } catch (e) {
    onFail(fileData);
  }
};

export default uploadReport;
