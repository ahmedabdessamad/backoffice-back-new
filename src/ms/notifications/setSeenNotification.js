// import onFail from "./onFail";
import { request, msConfigs } from "../helpers";

const setSeenNotification = async (type, timesheetID, id) => {
  try {
    const { host, secret } = msConfigs("MS_NOTIFICATION");
    const url = `${host}/seen/timesheets/${id}/${timesheetID}/${type}`;
    await request("patch", url, secret);
  } catch (e) {
    console.warn("set seen failure for ", timesheetID, id);
  }
};

export default setSeenNotification;
