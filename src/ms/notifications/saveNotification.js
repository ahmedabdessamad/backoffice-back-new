import onFail from "./onFail";
import { request, msConfigs } from "../helpers";

const saveNotification = async (type, timesheet, ids) => {
  const notifs = {
    cfUniqueIDs: ids,
    timesheetID: timesheet.id,
    placementID: timesheet.placementID,
    month: timesheet.month,
    year: timesheet.year,
    type
  };

  try {
    const { host, secret } = msConfigs("MS_NOTIFICATION");
    const url = `${host}/timesheets`;
    await request("post", url, secret, notifs);
  } catch (e) {
    onFail(notifs);
  }
};

export default saveNotification;
