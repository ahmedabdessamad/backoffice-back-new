import { msConfigs, request } from "../helpers";

const sendEmailService = async (
  service,
  action,
  timesheetId
) => {
  const { host, secret } = msConfigs("MS_MAILING");
  const url = `${host}/send/${service}/${action}/${timesheetId}`;
  console.log("url",url);
  return request("post", url, secret);

};

export default sendEmailService;
