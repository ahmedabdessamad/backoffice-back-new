import { msConfigs, request } from "../helpers";

const sendHtmlEmail = async (
  mailTo,
  subject,
  html,
  attachment,
  service,
  action
) => {
  const { host, secret } = msConfigs("MS_MAILING");
  const url = `${host}/send-html-email`;
  const emailObj = {
    mailTo,
    html,
    subject,
    attachment
  };
  return request("post", url, secret, emailObj, {
    service,
    action
  });
};

export default sendHtmlEmail;
