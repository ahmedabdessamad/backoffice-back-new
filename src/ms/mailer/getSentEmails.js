import { msConfigs, request } from "../helpers";

const getSentEmails = async (options) => {
  const { host, secret } = msConfigs("MS_MAILING");
  const url = `${host}/sent-emails`;
  const apiRes = await request("get", url, secret, null, options);
  return apiRes.data;
};

export default getSentEmails;
