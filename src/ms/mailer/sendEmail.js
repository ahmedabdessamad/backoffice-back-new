import { msConfigs, request } from "../helpers";
const sendEmail = async (
  mailTo,
  subject,
  templateID,
  vars,
  attachment,
  service,
  action
) => {
  const { host, secret } = msConfigs("MS_MAILING");
  const url = `${host}/send-template-email`;
  const emailObj = {
    mailTo,
    templateID,
    subject,
    vars,
    attachment
  };
  return request("post", url, secret, emailObj, {
    service,
    action
  });
};

export default sendEmail;
