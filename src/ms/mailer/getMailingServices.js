/**
 *
 * @returns {Promise<string[]>}
 */
const getMailingServices = async () => {
  return ["TIMESHEET", "CONTACT", "SUPPORT"];
};

module.exports = getMailingServices;
