import { msConfigs, request } from "../helpers";

const getExcludedEmails = async (options) => {
  const { host, secret } = msConfigs("MS_MAILING");
  const url = `${host}/excludes`;
  const apiRes = request("get", url, secret, null, options);
  return apiRes.data;
};

export default getExcludedEmails;
