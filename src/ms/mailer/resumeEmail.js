import { msConfigs, request } from "../helpers";

const resumeEmail = async (email) => {
  const { host, secret } = msConfigs("MS_MAILING");
  const url = `${host}/resume`;
  return request("post", url, secret, { email });
};

module.exports = resumeEmail;
