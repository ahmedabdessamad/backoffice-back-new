import { msConfigs, request } from "../helpers";

const excludeEmail = async (email, services) => {
  const { host, secret } = msConfigs("MS_MAILING");
  const url = `${host}/excludes`;
  return request("post", url, secret, { email, services });
};

export default excludeEmail;
