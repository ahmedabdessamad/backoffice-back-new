import { request, msConfigs } from "../helpers";

const getLogs = async (filter) => {
  const { host, secret } = msConfigs("MS_LOGS_HISTORY");
  const apiRes = await request("get", host, secret, null, filter);
  return apiRes.data;
};

export default getLogs;
