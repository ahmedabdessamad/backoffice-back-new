import { msConfigs, request } from "../helpers";

const requestTimesheet = async (method, path, body, params) => {
  const { host, secret } = msConfigs("MS_TIMESHEET");
 // console.log("{ host, secret }", host, secret);
  const url = `${host}${path}`;
  //console.log("url", url);
  const apiRes = await request(method, url, secret, body, params);
 // console.log("apiRes", apiRes);
  return apiRes.data;
};

export default requestTimesheet;
