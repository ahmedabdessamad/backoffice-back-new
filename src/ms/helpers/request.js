import axios from "axios";
import { genAuthToken } from "./index";

const request = async (method, url, secret, body, search) => {

  const token = genAuthToken(secret);
  const axiosConfig = {
    method,
    url,
    data: body,
    params: search,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Barear ${token}`,
      Accept: "application/json"
    }
  };
  return axios(axiosConfig);
};

export default request;
