const MS_CONFIGS_ERROR = new Error("ms logs configs error");

const msConfigs = (msName) => {
  const { __microservice_registry } = global;
  if (!__microservice_registry) throw MS_CONFIGS_ERROR;

  const ms = __microservice_registry[msName];
  if (!ms) throw MS_CONFIGS_ERROR;

  const { host, secret } = ms;
  if (!host || !secret) throw MS_CONFIGS_ERROR;

  return {
    host,
    secret
  };
};

export default msConfigs;
