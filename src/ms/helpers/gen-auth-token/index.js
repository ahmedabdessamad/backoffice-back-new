import { sign } from "jsonwebtoken";
import selfIdentification from "./selfIdentification";

const genAuthToken = (secret) => sign(selfIdentification, secret);

export default genAuthToken;
