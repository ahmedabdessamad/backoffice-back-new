import runVars from "../../../configs/runtimeVars";

const { MS_NAME, HOST } = runVars;

const SelfIdentification = {
  _name: MS_NAME,
  _host: HOST
};

export default SelfIdentification;
