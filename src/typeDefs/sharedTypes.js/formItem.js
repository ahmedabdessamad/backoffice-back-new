const formItem = {
  name: String,
  key: String,

  type: String,

  placeholder: String,

  defaultValue: String,

  required: Boolean,

  maxLength: String
};

module.exports = {
  formItem
};
