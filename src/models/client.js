const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ClientSchema = new Schema(
  {
    id: { type: Number, index: true }, //the id can't be set as unique index because of the null constraint in mogodb https://docs.mongodb.com/manual/core/index-unique/#unique-index-and-missing-field,
    accountId: { type: String, index: true, required: true, unique: true }, //This is the foreign key for account collection
    email: { type: String, index: true, required: true },
    companyName: String,
    companyID: { type: Number, default: 2023 }, // This a generic company specified by Felix to avoid BH validation
    activityArea: {
      type: [{ type: String }],
      default: []
    },
    currentPosition: String,
    sizeOfTheCompany: Number,
    isESN: Boolean,
    password: String,
    firstname: String,
    lastname: String,
    phonenumber: String,
    profilePhotoURL: String,
    preferredContact: String,
    receiveNewsletter: Boolean,
    customTextBlock1: String,
    source: String
  },
  { collection: "client", timestamps: true }
);

module.exports = mongoose.model("Client", ClientSchema);
