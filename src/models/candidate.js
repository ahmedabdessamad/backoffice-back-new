const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const candidateSchema = new Schema(
  {
    email: {
      type: String,
      required: true
    }

    /*MissedDeals: [
        {
            type: Schema.Types.ObjectId,
            ref: 'MissedDeal'
        }
    ]*/
  },
  { timestamps: true }
);

module.exports = mongoose.model("Candidate", candidateSchema);
