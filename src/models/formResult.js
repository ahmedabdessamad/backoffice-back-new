const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const formResultSchema = new Schema(
  {
    formID: {
      type: String,
      required: true
    },

    formTitle: {
      type: String,
      required: true
    },

    formCreator: {
      type: String,
      required: true
    },

    clientID: {
      type: String,
      required: true
    },

    answers: {
      type: Array,
      default: []
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("FormResult", formResultSchema);
