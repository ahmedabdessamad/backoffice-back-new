// This entity represents BullHorn JonSubmission
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const JobSubmissionSchema = new Schema(
  {
    id: { type: Number, index: true, required: true, unique: true },
    appointments: {
      total: { type: Number, default: 0 },
      data: {
        type: [],
        default: []
      }
    },
    billRate: { type: Number },
    candidate: {
      id: { type: Number },
      firstName: { type: String },
      lastName: { type: String }
    },
    comments: { type: String, default: "" },
    customText1: { type: String, default: "" },
    customText2: { type: String, default: "" },
    customText3: { type: String, default: "" },
    customText4: { type: String, default: "" },
    customText5: { type: String, default: "" },
    dateAdded: { type: Number },
    dateLastModified: { type: Number },
    dateWebResponse: { type: Number },
    isDeleted: { type: Boolean },
    isHidden: { type: Boolean },
    jobOrder: {
      id: { type: Number },
      title: { type: String }
    },
    latestAppointment: {
      id: { type: Number }
    },
    migrateGUID: { type: Number },
    owners: {
      total: { type: Number },
      data: { type: [], default: [] }
    },
    payRate: { type: Number },
    salary: { type: Number },
    sendingUser: {
      id: { type: Number },
      _subtype: { type: String, default: "" },
      firstName: { type: String, default: "" },
      lastName: { type: String, default: "" }
    },
    source: { type: String, default: "" },
    status: { type: String, default: "" },
    tasks: {
      total: { type: Number, default: 0 },
      data: { type: [], default: [] }
    },
    validationStatus: String
  },
  { collection: "bhJobSubmission" }
);

module.exports = mongoose.model("jobSubmission", JobSubmissionSchema);
