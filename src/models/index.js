const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const models = {};

models.mongoose = mongoose;

models.User = require("./user");
models.Role = require("./role");
models.Group = require("./group");
models.tokenSet = require("./tokenSet");
models.batchPayment = require("./batchPayment");
models.Sepa = require("./sepa");
models.Hsbc = require("./hsbc");
models.CorporateUser = require("./corporateUser");
models.MissedDeal = require("./missedDeal");
models.MarketInfo = require("./marketInfo");
models.Candidate = require("./candidate");
models.CDO = require("./cdo");
models.DataManager = require("./dataManager");
models.MarketingManager = require("./marketingManager");
models.Sales = require("./sales");
models.Sourceur = require("./sourceur");
models.BusinessSector = require("./businessSector");
models.ClientCorporation = require("./clientCorporation");
models.Category = require("./category");
models.JobSubmission = require("./jobSubmission");
models.ValidatedJobSubmission = require("./validatedJobSubmission");
models.Form = require("./form");
models.FormResult = require("./formResult");
models.Permission = require("./permission");

models.TokenReset = require("./tokenReset");
models.Cron = require("./cron");
models.invoice = require("./invoice");
module.exports = models;
