const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CategorySchema = new Schema(
  {
    name: String,
    skills: {
      total: Number,
      data: [{ id: Number, name: String }]
    }
  },
  {
    collection: "category"
    //collection: "Categories"
  }
);

module.exports = mongoose.model("Category", CategorySchema);
