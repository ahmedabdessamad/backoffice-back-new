const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const StatusEnum = ["DRAFT", "SUBMITTED", "AUTHORISED"];
const InvoiceSchema = Schema(
  {
    type: String,
    contact: {
      contactID: String,
      contactStatus: String,
      name: String,
      addresses: [{}],
      phones: [{}],
      updatedDateUTC: String,
      isSupplier: String,
      isCustomer: String
    },
    date: String,
    dueDate: String,
    dateString: Date,
    dueDateString: Date,
    status: {
      type: String,
      enum: StatusEnum
    },
    lineAmountTypes: String,
    lineItems: [{}],
    subTotal: String,
    totalTax: String,
    total: String,
    updatedDateUTC: String,
    currencyCode: String,
    invoiceID: String,
    invoiceNumber: String,
    payments: [{}],
    amountDue: String,
    amountPaid: String,
    amountCredited: String,
    tenant: String
  },
  {
    timestamps: true
  }
);
module.exports = mongoose.model("Invoice", InvoiceSchema);
