const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const AccountSchema = new Schema(
  {
    email: String,
    password: String,
    name: String,
    active: Boolean,
    role: String,
    firstname: String,
    lastname: String,
    phone: String,
    forgottenPasswordToken: String,
    cfWebsite: String,
    isVerified: { type: Boolean }
  },
  {
    collection: "account",
    timestamps: true
  }
);
module.exports = mongoose.model("Account", AccountSchema);
