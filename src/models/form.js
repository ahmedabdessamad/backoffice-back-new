const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const formSchema = new Schema(
  {
    subject: {
      type: String,
      required: true
    },

    structure: {
      type: Array,
      default: []
    },

    creator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Form", formSchema);
