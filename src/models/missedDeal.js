const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const missedDealSchema = new Schema(
  {
    companyId: {
      type: String,
      required: true
    },
    companyName: {
      type: String,
      required: true
    },

    /*comp: {
        type: Schema.Types.ObjectId,
        ref: "ClientCorporation"
    },*/
    ITConsultingFirm: {
      type: String,
      required: false
    },
    intermedianryAgency: {
      type: String,
      required: false
    },
    missionStart: {
      type: Date,
      required: true
    },
    missionEnd: {
      type: Date,
      required: true
    },
    skills: [{ id: Number, name: String }],
    jobTitle: {
      type: String,
      required: true
    },
    dailyRate: {
      type: String,
      required: true
    },
    city: {
      type: String,
      required: true
    },
    missionProjectDescription: {
      type: String,
      required: true
    },
    creator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Sourceur"
    },
    candidate: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("MissedDeal", missedDealSchema);
