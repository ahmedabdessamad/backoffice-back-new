const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const sourceurSchema = new Schema(
  {
    email: {
      type: String,
      required: true
    },

    userID: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Sourceur", sourceurSchema);
