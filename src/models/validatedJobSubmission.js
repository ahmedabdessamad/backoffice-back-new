const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const validatedJobSubmissionSchema = new Schema(
  {
    jobSubmissionID: { type: Number, required: true },
    jobOrderID: { type: Number, required: true },
    jobOrderTitle: { type: String, default: "" },
    status: {
      type: String,
      default: "cv sent"
    },
    candidateID: { type: Number, required: true },
    candidateFirstName: { type: String, default: "" },
    candidateLastName: { type: String, default: "" },
    candidateOccupation: { type: String, default: "" },
    candidateAvailableDate: Number,
    candidateAddress: {},
    candidateCv: { type: String, required: true },
    comment: { type: String, default: "" },
    payRate: Number,
    reference: {
      type: [
        {
          id: Number,
          referenceFirstName: String,
          referenceLastName: String,
          referenceTitle: String,
          outcome: String
        }
      ],
      default: []
    },
    clientComment: { type: String, default: "" }
  },
  { collection: "validatedJobSubmission", timestamp: true }
);

module.exports = mongoose.model(
  "validatedJobSubmissionSchema",
  validatedJobSubmissionSchema
);
