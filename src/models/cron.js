const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CronSchema = new Schema(
  {
    organisation: { type: String, default: "" },
    error: { type: Boolean, default: false },
    cronDate: { type: Number, default: Date.now() }
  },
  {
    collection: "cron",
    timestamps: true
  }
);

module.exports = mongoose.model("Cron", CronSchema);
