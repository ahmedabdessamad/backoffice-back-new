const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TokenResetSchema = Schema(
  {
    user: { type: mongoose.Schema.Types.ObjectId, required: true, ref: "User" },
    reset_password_token: {
      type: String
    },
    reset_password_expires: {
      type: Date
    }
  },
  { timestamps: true }
);
module.exports = mongoose.model("TokenReset", TokenResetSchema);
