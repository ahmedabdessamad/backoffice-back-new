const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const HsbcSchema = Schema({
  batchPaymentID: { type: String, unique: true, required: true },
  data: {
    paymentInformationId: {
      type: "String"
    },
    creationDateTime: {
      type: "Date"
    },
    numberOfTransactions: {
      type: "Number"
    },
    initiatingParty: {
      name: {
        type: "String"
      },
      postalAddress: {
        country: {
          type: "String"
        },
        addressLine: {
          type: ["String"]
        }
      },
      organisationId: {
        identification: {
          type: "String"
        },
        schemeName: {
          type: "String"
        },
        issuer: {
          type: "String"
        }
      }
    },
    paymentTypeInformation: {
      serviceLevel: {
        type: "String"
      },
      localInstrument: {
        type: "String"
      },
      categoryPurpose: {
        type: "String"
      }
    },
    debtor: {
      name: {
        type: "String"
      },
      postalAddress: {
        country: {
          type: "String"
        },
        addressLine: {
          type: ["String"]
        }
      },
      privateId: {
        identification: {
          type: "String"
        },
        schemeName: {
          type: "String"
        },
        issuer: {
          type: "String"
        }
      }
    },
    creditor: {
      name: {
        type: "String"
      },
      postalAddress: {
        country: {
          type: "String"
        },
        addressLine: {
          type: ["String"]
        }
      },
      organisationId: {
        identification: {
          type: "String"
        },
        schemeName: {
          type: "String"
        },
        issuer: {
          type: "String"
        }
      }
    },
    creditorAccount: {
      iban: {
        type: "String"
      }
    },
    ultimateCreditor: {
      name: {
        type: "String"
      },
      postalAddress: {
        country: {
          type: "String"
        },
        addressLine: {
          type: ["String"]
        }
      },
      organisationId: {
        identification: {
          type: "String"
        },
        schemeName: {
          type: "String"
        },
        issuer: {
          type: "String"
        }
      }
    },
    purpose: {
      type: "String"
    },
    chargeBearer: {
      type: "String"
    },
    creditTransferTransaction: {
      type: ["Mixed"]
    },
    supplementaryData: {
      acceptedAuthenticationApproach: {
        type: ["String"]
      },
      successfulReportUrl: {
        type: "String"
      },
      unsuccessfulReportUrl: {
        type: "String"
      }
    }
  }
});
module.exports = mongoose.model("Hsbc", HsbcSchema);
