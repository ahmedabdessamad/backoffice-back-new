const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const tokenSetSchema = Schema(
  {
    id_token: {
      type: String
    },
    access_token: {
      type: String
    },
    expires_in: {
      type: Number
    },
    expires_at: {
      type: Number
    },
    token_type: { type: String },
    refresh_token: {
      type: String,
      unique: true,
      required: true
    },
    scope: {
      type: String
    },
    state: {
      type: String
    },
    tenants: [{}]
  },
  { timestamps: true }
);

module.exports = mongoose.model("tokenSet", tokenSetSchema);
