const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PermissionSchema = new Schema(
  {
    name: {
      type: String
    },
    tabs: [
      {
        name: String,
        permission: Number,
        parent: String
      }
    ]
  },
  { collection: "Permissions", timestamps: true }
);

module.exports = mongoose.model("Permission", PermissionSchema);
