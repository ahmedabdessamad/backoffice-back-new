const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const dataManagerSchema = new Schema(
  {
    email: {
      type: String,
      required: true
    },

    userID: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("DataManager", dataManagerSchema);
