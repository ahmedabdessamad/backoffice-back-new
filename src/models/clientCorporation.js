const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ClientCorporationSchema = new Schema(
  {
    /*id: {
        type: Number,
        required: true
      },*/

    name: {
      type: String,
      required: true
    }
  },
  {
    // collection: "clientCorporation"
    collection: "ClientCorporation"
  }
);

module.exports = mongoose.model("ClientCorporation ", ClientCorporationSchema);
