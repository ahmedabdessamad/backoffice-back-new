const mongoose = require("mongoose");

const { notifyOnNewBatchPayment } = require("../helpers/emailFns");

const Schema = mongoose.Schema;

const types = ["PAYBATCH", "RECBATCH"];
const status = ["AUTHORISED", "DELETED"];
const validation = ["PENDING", "PAID", "REJECTED", "EXPORTED"];

const batchPaymentSchema = new Schema(
  {
    account: {},
    reference: String,
    particulars: String,
    code: String,
    details: String,
    narrative: String,
    batchPaymentID: { type: String, unique: true, required: true },
    dateString: String,
    date: Date,
    amount: Number,
    payments: [{}],
    type: {
      type: String,
      enum: types
    },
    status: {
      type: String,
      enum: status
    },
    validationStatus: {
      type: String,
      enum: validation,
      default: "PENDING"
    },
    totalAmount: String,
    updatedDateUTC: String,
    isReconciled: Boolean,
    organisation: String,
    tenant: String
  },
  {
    collection: "newBatchPayments",
    timestamps: true
  }
);

batchPaymentSchema.post("save", (doc) => {
  notifyOnNewBatchPayment(doc);
});

module.exports = mongoose.model("batchPayment", batchPaymentSchema);
