const itemShape = {
  ItemID: { type: String },
  Code: { type: String, required: true, unique: true },
  Name: String,
  PurchaseDescription: String,
  PurchaseDetails: Object,
  SalesDetails: Object,
  UpdatedDateUTC: String,
  placementID: { type: Number },
  cfEntity: { type: String }
};

export default itemShape;
