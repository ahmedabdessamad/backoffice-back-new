import { Schema, model } from "mongoose";
import itemShape from "./shape";

const Item = new Schema(itemShape, {
  collection: "items",
  timestamps: { currentTime: () => Date.now() },
  strict: true
});

export default model("Item", Item);
