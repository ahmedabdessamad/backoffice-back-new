const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const CorporateUserSchema = new Schema(
  {
    id: { type: Number, required: true, unique: true, index: true },
    address: {
      address1: { type: String, default: "" },
      address2: { type: String, default: "" },
      city: { type: String, default: "" },
      state: { type: String, default: "" },
      zip: { type: String, default: "" },
      countryID: { type: Number, default: 0 },
      countryName: { type: String, default: "" },
      countryCode: { type: String, uppercase: true, default: "" }
    },
    branch: {},
    branches: {
      total: { type: String, default: 0 },
      data: []
    },
    companyName: { type: String, default: "" },
    customDate1: { type: Date, default: null },
    customDate2: { type: Date, default: null },
    customDate3: { type: Date, default: null },
    customFloat1: { type: Number, default: 0 },
    customFloat2: { type: Number, default: 0 },
    customFloat3: { type: Number, default: 0 },
    customInt1: { type: Number, default: 0 },
    customInt2: { type: Number, default: 0 },
    customInt3: { type: Number, default: 0 },
    customText1: { type: String, default: "" },
    customText10: { type: String, default: "" },
    customText11: { type: String, default: "" },
    customText12: { type: String, default: "" },
    customText13: { type: String, default: "" },
    customText14: { type: String, default: "" },
    customText15: { type: String, default: "" },
    customText16: { type: String, default: "" },
    customText17: { type: String, default: "" },
    customText18: { type: String, default: "" },
    customText19: { type: String, default: "" },
    customText2: { type: String, default: "" },
    customText20: { type: String, default: "" },
    customText3: { type: String, default: "" },
    customText4: { type: String, default: "" },
    customText5: { type: String, default: "" },
    customText6: { type: String, default: "" },
    customText7: { type: String, default: "" },
    customText8: { type: String, default: "" },
    customText9: { type: String, default: "" },
    dateLastComment: { type: String, default: "" },
    delegations: {
      total: { type: Number, default: 0 },
      data: []
    },
    departmentIdList: {},
    departments: {
      total: { type: Number, default: 0 },
      data: []
    },
    email: { type: String, default: "" },
    email2: { type: String, default: "" },
    email3: { type: String, default: "" },
    emailNotify: { type: Boolean, default: true },
    emailSignature: { type: String, default: "" },
    enabled: { type: Boolean, default: true },
    externalEmail: { type: String, default: "" },
    fax: { type: String, default: "" },
    fax2: { type: String, default: "" },
    fax3: { type: String, default: "" },
    firstName: { type: String, default: "" },
    inboundEmailEnabled: { type: Boolean, default: true },
    isAnonymized: { type: Boolean, default: false },
    isDayLightSavings: { type: Boolean, default: false },
    isDeleted: { type: Boolean, default: false },
    isHidden: { type: Boolean, default: false },
    isLockedOut: { type: Boolean, default: false },
    isOutboundFaxEnabled: { type: Boolean, default: true },
    jobAssignments: {
      total: { type: Number, default: 0 },
      data: []
    },
    lastName: { type: String, default: "" },
    loginRestrictions: {
      ipAddress: { type: String },
      timeStart: { type: String },
      timeEnd: { type: String },
      weekDays: [String]
    },
    massMailOptOut: { type: Boolean, default: true },
    masterUserID: { type: String, default: "" },
    middleName: { type: String, default: "" },
    mobile: { type: String, default: "" },
    name: { type: String, default: "" },
    namePrefix: { type: String, default: "Mrs" },
    nameSuffix: { type: String, default: "" },
    nickName: { type: String, default: "" },
    occupation: { type: String, default: "" },
    pager: { type: String, default: "" },
    personSubtype: { type: String, default: "" },
    phone: { type: String, default: "" },
    phone2: { type: String, default: "" },
    phone3: { type: String, default: "" },
    primaryDepartment: {
      id: { type: Number, default: null },
      name: { type: String, default: "" }
    },
    reportToPerson: { type: Object, default: {} },
    status: { type: String, default: "" },
    taskAssignments: {
      total: { type: Number, default: 0 },
      data: []
    },
    timeZoneOffsetEST: { type: Number, default: 0 },
    userDateAdded: { type: Number, default: 0 },
    userType: {
      id: { type: Number, default: null },
      name: { type: String, default: "" }
    },
    username: { type: String, default: "" }
  },
  {
    collection: "corporateUser",
    timestamps: true
  }
);

module.exports = mongoose.model("CorporateUser", CorporateUserSchema);
