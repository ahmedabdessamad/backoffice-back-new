const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const SepaSchema = Schema({
  batchPaymentID: { type: String, unique: true, required: true },
  Document: {
    "@xmlns": {
      type: "String"
    },
    CstmrCdtTrfInitn: {
      GrpHdr: {
        MsgId: {
          type: "String"
        },
        CreDtTm: {
          type: "Date"
        },
        NbOfTxs: {
          type: "String"
        },
        CtrlSum: {
          type: "String"
        },
        InitgPty: {
          Id: {
            OrgId: {
              Othr: {
                Id: {
                  type: "String"
                }
              }
            }
          }
        }
      },
      PmtInf: {
        PmtInfId: {
          type: "String"
        },
        PmtMtd: {
          type: "String"
        },
        PmtTpInf: {
          SvcLvl: {
            Cd: {
              type: "String"
            }
          }
        },
        ReqdExctnDt: {
          type: "String"
        },
        Dbtr: {
          Nm: {
            type: "String"
          },
          PstlAdr: {
            StrtNm: {
              type: "String"
            },
            BldgNb: {
              type: "String"
            },
            PstCd: {
              type: "String"
            },
            TwnNm: {
              type: "String"
            },
            CtrySubDvsn: {
              type: "String"
            },
            Ctry: {
              type: "String"
            }
          }
        },
        DbtrAcct: {
          Id: {
            IBAN: {
              type: "String"
            }
          }
        },
        DbtrAgt: {
          FinInstnId: {
            BIC: {
              type: "String"
            },
            PstlAdr: {
              Ctry: {
                type: "String"
              }
            }
          }
        },
        ChrfBr: {
          type: "String"
        },
        CdtTrfTxInf: {
          PmtId: {
            InstrId: {
              type: "String"
            },
            EndToEndId: {
              type: "String"
            }
          }
        }
      }
    }
  }
});
module.exports = mongoose.model("Sepa", SepaSchema);
