const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BusinessSectorSchema = new Schema(
  {
    dateAdded: Number,
    name: String
  },
  {
    collection: "businessSector"
    //collection: "businesssectors"
  }
);

module.exports = mongoose.model("BusinessSector", BusinessSectorSchema);
