const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema(
  {
    username: {
      type: String
    },
    bhID: { type: Number },
    password: {
      type: String,
      required: true
    },
    email: {
      type: String,
      unique: true,
      required: true
    },
    active: {
      type: Boolean
    },
    permissions: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Permission"
      }
    ]
  },
  { collection: "cockpitUsers", timestamps: true }
);

// export model user with UserSchema
module.exports = mongoose.model("User", UserSchema);
