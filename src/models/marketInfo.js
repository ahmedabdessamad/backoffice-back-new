const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const marketInfoSchema = new Schema(
  {
    companyId: {
      type: String,
      required: true
    },
    companyName: {
      type: String,
      required: true
    },

    jobTitle: {
      type: String,
      required: true
    },
    contact: {
      type: String,
      required: true
    },
    contactName: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    phone: {
      type: String,
      required: true
    },
    industry: [
      {
        type: Schema.Types.ObjectId,
        ref: "BusinessSector"
      }
    ],
    categories: [
      {
        type: Schema.Types.ObjectId,
        ref: "Categorie"
      }
    ],
    description: {
      type: String,
      required: true
    },
    creator: {
      type: Schema.Types.ObjectId,
      ref: "Sourceur"
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("MarketInfo", marketInfoSchema);
