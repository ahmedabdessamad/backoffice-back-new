import "regenerator-runtime/runtime.js";
import express from "express";
import cors from "cors";
import { json } from "body-parser";
import { ApolloServer } from "apollo-server-express";
import { scheduleJob } from "node-schedule";
import typeDefs from "./typeDefs";
import resolvers from "./resolvers";
import runtimeVars from "./configs/runtimeVars";
import dbConnect from "./db/dbConnect";
const { emailSending } = require("./resolvers/missedDealHelpers");

const getBatchPayments = require("./helpers/xeroHelpers/getBatchPayments");
import context from "./middlewares/context";
const { initialCollections } = require("./helpers/initialCollections");
const externalRoutes = require("./routes/externalRoutes");
const reportRoutes = require("./routes/reportRoutes");
import register from "./ms/register";
import startJobs from "./crones";
import xeroItemSynchroJob from "./crones/xeroItemSynchroJob";

const { PORT, NODE_ENV } = runtimeVars;

const app = express();
app.use(/\/((?!webhook).)*/, cors(), json());

const server = new ApolloServer({
  introspection: true,
  playground: NODE_ENV !== "production",
  typeDefs,
  resolvers,
  context
});

server.applyMiddleware({ app, path: "/graphql" });

app.use("/files", express.static(__dirname + "/../files/"));
app.use("/", externalRoutes);
app.use("/", reportRoutes);

app.listen(PORT, async () => {
  await dbConnect();
  await register();
  console.info(`Server is running on port ${PORT}`);
  initialCollections();
  //  startJobs();
  // try {
  //   // await xeroItemSynchroJob();
  //   // if (process.env.NODE_ENV !== "development") {
  //   //   await getBatchPayments();
  //   }
  // } catch (e) {
  //   console.log("finance", e);
  // }
  // try {
  //   scheduleJob("* y/7", () => {
  //     emailSending();
  //   });
  // } catch (e) {}
});
